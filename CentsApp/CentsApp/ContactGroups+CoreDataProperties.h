//
//  ContactGroups+CoreDataProperties.h
//  CentsApp
//
//  Created by Sagar Jadhav on 1/25/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ContactGroups.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactGroups (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *parseObjectId;
@property (nullable, nonatomic, retain) NSString *groupName;
@property (nullable, nonatomic, retain) NSString *owner;
@property (nullable, nonatomic, retain) NSData *memberIds;

@end

NS_ASSUME_NONNULL_END
