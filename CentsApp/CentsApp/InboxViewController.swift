//
//  InboxViewController.swift
//  CentsApp
//
//  Created by Mahesh on 11/3/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import Parse


class InboxViewController: UIViewController {
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate
    var selectedOptionAlready:Int = 0
    var items: [PFObject]! = []
    var answerArray: [NSObject]! = []
    let btn   = UIButton(type: UIButtonType.System)
    let btn1   = UIButton(type: UIButtonType.System)
    var allQuestionsModel: [QuestionModel]! = []
    var selectedIndex:Int = 0
    var GlobalquestionObjectITVC:QuestionModel!
    var GLobalanswerObjectITVC:AnswerModel!
    
    @IBOutlet var lblTimerElapsed: UILabel!
    @IBOutlet var btnTimerIndicator: UIButton!
    
    @IBOutlet weak var option4: UIButton!
    @IBOutlet weak var option1: UIButton!
    @IBOutlet weak var option2: UIButton!
    @IBOutlet weak var option3: UIButton!
    @IBOutlet weak var imageBtn2: UIButton!
    @IBOutlet weak var imageBtn1: UIButton!
    @IBOutlet weak var labelQuestion: UILabel!
    
    @IBOutlet weak var ImageBtn4: UIButton!
    @IBOutlet weak var ImageBtn3: UIButton!
    
    @IBOutlet weak var lblOptionOneCount: UILabel!
    @IBOutlet weak var lblOptionTwoCount: UILabel!
    @IBOutlet weak var lblOptionThreeCount: UILabel!
    @IBOutlet weak var lblOptionFourCount: UILabel!
    
    var globalDate:NSDate!
    var isQuestionExpired:Bool = false;
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btn.frame = CGRectMake(50, 5, self.view.frame.size.width-50,30)
        btn.setTitle("Inbox", forState: .Normal)  //set button title
        btn.setTitleColor(UIColor.whiteColor(),forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.addTarget(self, action: "ButtonInboxTapped:", forControlEvents: .TouchUpInside) //add button action
        //self.view.addSubview(btn)
        
        btn1.frame = CGRectMake(0, 15,50,30)
        btn1.setTitle("Back", forState: .Normal)  //set button title
        btn1.setTitleColor(UIColor.blackColor(),forState: .Normal)
        btn1.titleLabel?.font = UIFont.boldSystemFontOfSize(14)
        btn1.addTarget(self, action: "ButtonBackTapped:", forControlEvents: .TouchUpInside) //add button action
        self.view.addSubview(btn1)
        
        let maskPath =
        UIBezierPath(roundedRect:self.view.bounds, byRoundingCorners: [UIRectCorner.TopLeft , UIRectCorner.TopRight], cornerRadii: CGSizeMake(10.0, 10.0))
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.CGPath
        self.view.layer.mask = maskLayer
        //self.view.addSubview(btn)
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGestureLeft:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGestureRight:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        
        // GlobalquestionObjectITVC is the current question object
        GlobalquestionObjectITVC = self.allQuestionsModel[selectedIndex]
        self.labelQuestion.text = GlobalquestionObjectITVC.question as? String
        self.btnTimerIndicator.layer.cornerRadius = 12.5
        self.showQuestion() // refresh each question
        
    }
    
    func showQuestion(){
        self.checkAnswersforAllUsers() // this method is checks all ansers for the current question 
        
        
        //*****timer button show red - going to expire  green - available or grey - expired
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd/MM/yyyyy hh:mm a"
        print("question detail::\(GlobalquestionObjectITVC.expiryDate)");
        
        let str:String = GlobalquestionObjectITVC.expiryDate as! String
        let dateobj = dateFormatter.dateFromString(str)
        if dateobj!.compare(NSDate()) == NSComparisonResult.OrderedDescending
        { // question is active
            self.btnTimerIndicator.backgroundColor = UIColor(red: 26/255, green: 104/255, blue: 23/255, alpha: 1.0)
            self.option1.enabled = true
            self.option2.enabled = true
            self.option3.enabled = true
            self.option4.enabled = true
            self.imageBtn1.enabled = true
            self.imageBtn2.enabled = true
            self.ImageBtn3.enabled = true
            self.ImageBtn4.enabled = true
            
            //check need to turn red or not
            var dateobjInner = dateFormatter.dateFromString(str)
            var hrsToAdd:Double = 1
            if ((self.appDelegateObj.settingObject) != nil){
                hrsToAdd = (self.appDelegateObj.settingObject.redExpiryTime?.doubleValue)!;
            }
            else{
                hrsToAdd = 1
            }
            dateobjInner = dateobjInner?.dateByAddingTimeInterval(-60*60*hrsToAdd)
            if dateobjInner!.compare(NSDate()) == NSComparisonResult.OrderedAscending
            {
                self.btnTimerIndicator.backgroundColor = UIColor.redColor()
            }
            
            self.globalDate = dateobj
            self.btnTimerIndicator.userInteractionEnabled = true
            self.isQuestionExpired = false
            
            
        }
        else{ // question expired
            self.btnTimerIndicator.backgroundColor = UIColor.blueColor()
            self.option1.enabled = false
            self.option2.enabled = false
            self.option3.enabled = false
            self.option4.enabled = false
            self.imageBtn1.enabled = false
            self.imageBtn2.enabled = false
            self.ImageBtn3.enabled = false
            self.ImageBtn4.enabled = false
            
            self.btnTimerIndicator.userInteractionEnabled = false
            self.isQuestionExpired = true
        }
        
       
        
        self.option1.backgroundColor = UIColor.clearColor()
        self.option2.backgroundColor = UIColor.clearColor()
        self.option3.backgroundColor = UIColor.clearColor()
        self.option4.backgroundColor = UIColor.clearColor()
        self.imageBtn1.layer.borderColor = UIColor.clearColor().CGColor
        self.imageBtn2.layer.borderColor = UIColor.clearColor().CGColor
        self.ImageBtn3.layer.borderColor = UIColor.clearColor().CGColor
        self.ImageBtn4.layer.borderColor = UIColor.clearColor().CGColor
        
        // according to question type set ansers for text buttons or image buttons
        if GlobalquestionObjectITVC.isType == 1 {
            option1.setTitle("\(GlobalquestionObjectITVC.textOptionOne as NSString!) ", forState: UIControlState.Normal)
            option2.setTitle("\(GlobalquestionObjectITVC.textOptionTwo as NSString!)", forState: UIControlState.Normal)
            option3.setTitle("\(GlobalquestionObjectITVC.textOptionThree as NSString!)", forState: UIControlState.Normal)
            option4.setTitle("\(GlobalquestionObjectITVC.textOptionFour as NSString!)", forState: UIControlState.Normal)
            self.imageBtn1.setBackgroundImage(nil, forState: UIControlState.Normal)
            self.imageBtn2.setBackgroundImage(nil, forState: UIControlState.Normal)
            self.ImageBtn3.setBackgroundImage(nil, forState: UIControlState.Normal)
            self.ImageBtn4.setBackgroundImage(nil, forState: UIControlState.Normal)
        }
        else{
            option1.setTitle("", forState: UIControlState.Normal)
            option2.setTitle("", forState: UIControlState.Normal)
            option3.setTitle("", forState: UIControlState.Normal)
            option4.setTitle("", forState: UIControlState.Normal)
            
            if let userPicture = GlobalquestionObjectITVC.imageOptionOne {
                userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                    if (error == nil) {
                        self.imageBtn1.setBackgroundImage(UIImage(data:imageData!), forState: UIControlState.Normal)
                    }
                }
            }
            if let userPicture = GlobalquestionObjectITVC.imageOptionTwo{
                userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                    if (error == nil) {
                        self.imageBtn2.setBackgroundImage(UIImage(data:imageData!), forState: UIControlState.Normal)
                    }
                }
            }
            if let userPicture = GlobalquestionObjectITVC.imageOptionThree {
                userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                    if (error == nil) {
                        self.ImageBtn3.setBackgroundImage(UIImage(data:imageData!), forState: UIControlState.Normal)
                    }
                }
            }
            if let userPicture = GlobalquestionObjectITVC.imageOptionFour{
                userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                    if (error == nil) {
                        self.ImageBtn4.setBackgroundImage(UIImage(data:imageData!), forState: UIControlState.Normal)
                    }
                }
            }
        }
        
        
        self.getSelectedAnswerAlready() // if you have given anser already for the current question it will reflect on the screen
    }
    func respondToSwipeGestureRight(gesture:UIGestureRecognizer){
        if self.selectedIndex == 0{
           return
        }
        self.selectedIndex--;
        GlobalquestionObjectITVC = self.allQuestionsModel[selectedIndex]
        self.labelQuestion.text = GlobalquestionObjectITVC.question as? String
        self.showQuestion()  // refresh each question
    }
    
    
    func respondToSwipeGestureLeft(gesture:UIGestureRecognizer){
        if self.selectedIndex == (self.allQuestionsModel.count-1){
            return
        }
        
        self.selectedIndex++;
        GlobalquestionObjectITVC = self.allQuestionsModel[selectedIndex]
        self.labelQuestion.text = GlobalquestionObjectITVC.question as? String
        
        self.showQuestion() // refresh each question
    }
    
    
    
    
    // if you have given anser already for the current question it will reflect on the screen
    func getSelectedAnswerAlready()
    {
        let compQuery = PFQuery(className:"AnswerNew")
        compQuery.whereKey("userId", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
        compQuery.whereKey("QuestionID", equalTo:self.GlobalquestionObjectITVC.objectId!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if results?.count != 0{
                    
                    let objects = results as? [PFObject]
                    let obj:PFObject = objects![0]
                    self.selectedOptionAlready = obj.objectForKey("SelectedOption") as! Int
                    if self.GlobalquestionObjectITVC.isType == 1{
                        if self.selectedOptionAlready == 1
                        {
                            self.option1.backgroundColor = UIColor.lightGrayColor()
                        }
                        if self.selectedOptionAlready == 2
                        {
                            self.option2.backgroundColor = UIColor.lightGrayColor()
                        }
                        if self.selectedOptionAlready == 3
                        {
                            self.option3.backgroundColor = UIColor.lightGrayColor()
                        }
                        if self.selectedOptionAlready == 4
                        {
                            self.option4.backgroundColor = UIColor.lightGrayColor()
                        }
                        
                      
                        
                    }
                    if self.GlobalquestionObjectITVC.isType == 2{
                        if self.selectedOptionAlready == 1
                        {
                            self.imageBtn1.layer.borderWidth = 5
                            self.imageBtn1.layer.borderColor = UIColor.lightGrayColor().CGColor
                            
                            
                        }
                        if self.selectedOptionAlready == 2
                        {
                            self.imageBtn2.layer.borderWidth = 5
                            self.imageBtn2.layer.borderColor = UIColor.lightGrayColor().CGColor
                        }
                        if self.selectedOptionAlready == 3
                        {
                            self.ImageBtn3.layer.borderWidth = 5
                            self.ImageBtn3.layer.borderColor = UIColor.lightGrayColor().CGColor

                        }
                        if self.selectedOptionAlready == 4
                        {
                            self.ImageBtn4.layer.borderWidth = 5
                            self.ImageBtn4.layer.borderColor = UIColor.lightGrayColor().CGColor
                        }
                        
                    }
                }
            }
            else{
                print("Error in retrieving \(error)")
            }
            
        })
    }
    func ButtonInboxTapped(sender:UIButton!)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("InboxTapped", object: nil)
    }
    func ButtonBackTapped(sender:UIButton!)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
 
    @IBAction func option4Tapped(sender: AnyObject)
    {
        self.option1.backgroundColor = UIColor.clearColor()
        self.option2.backgroundColor = UIColor.clearColor()
        self.option3.backgroundColor = UIColor.clearColor()
        self.option4.backgroundColor = UIColor.lightGrayColor()
        self.parseCallToPostAnswer(sender as! UIButton)
    }
    
    @IBAction func option3Tapped(sender: AnyObject)
    {
        self.option1.backgroundColor = UIColor.clearColor()
        self.option2.backgroundColor = UIColor.clearColor()
        self.option3.backgroundColor = UIColor.lightGrayColor()
        self.option4.backgroundColor = UIColor.clearColor()
        self.parseCallToPostAnswer(sender as! UIButton)
    }
    @IBAction func option2Tapped(sender: AnyObject)
    {
        self.option1.backgroundColor = UIColor.clearColor()
        self.option2.backgroundColor = UIColor.lightGrayColor()
        self.option3.backgroundColor = UIColor.clearColor()
        self.option4.backgroundColor = UIColor.clearColor()
        self.parseCallToPostAnswer(sender as! UIButton)
    }
    @IBAction func option1Tapped(sender: AnyObject)
    {
        self.option1.backgroundColor = UIColor.lightGrayColor()
        self.option2.backgroundColor = UIColor.clearColor()
        self.option3.backgroundColor = UIColor.clearColor()
        self.option4.backgroundColor = UIColor.clearColor()
        self.parseCallToPostAnswer(sender as! UIButton)
    }
    @IBAction func imageBtn3Tapped(sender: AnyObject)
    {
        imageBtn1.layer.borderWidth = 5
        imageBtn1.layer.borderColor = UIColor.clearColor().CGColor
        imageBtn2.layer.borderWidth = 5
        imageBtn2.layer.borderColor = UIColor.clearColor().CGColor
        ImageBtn3.layer.borderWidth = 5
        ImageBtn3.layer.borderColor = UIColor.lightGrayColor().CGColor
        ImageBtn4.layer.borderWidth = 5
        ImageBtn4.layer.borderColor = UIColor.clearColor().CGColor
        
        
        self.parseCallToPostAnswer(sender as! UIButton)

    }
    
    @IBAction func imageBtn4Tapped(sender: AnyObject)
    {
        imageBtn1.layer.borderWidth = 5
        imageBtn1.layer.borderColor = UIColor.clearColor().CGColor
        imageBtn2.layer.borderWidth = 5
        imageBtn2.layer.borderColor = UIColor.clearColor().CGColor
        ImageBtn3.layer.borderWidth = 5
        ImageBtn3.layer.borderColor = UIColor.clearColor().CGColor
        ImageBtn4.layer.borderWidth = 5
        ImageBtn4.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.parseCallToPostAnswer(sender as! UIButton)

    }
    @IBAction func imageBtn1Tapped(sender: AnyObject)
    {
        imageBtn1.layer.borderWidth = 5
        imageBtn1.layer.borderColor = UIColor.lightGrayColor().CGColor
        imageBtn2.layer.borderWidth = 5
        imageBtn2.layer.borderColor = UIColor.clearColor().CGColor
        ImageBtn3.layer.borderWidth = 5
        ImageBtn3.layer.borderColor = UIColor.clearColor().CGColor
        ImageBtn4.layer.borderWidth = 5
        ImageBtn4.layer.borderColor = UIColor.clearColor().CGColor
        
        self.parseCallToPostAnswer(sender as! UIButton)

    }
    @IBAction func imageBtn2Tapped(sender: AnyObject)
    {
        imageBtn1.layer.borderWidth = 5
        imageBtn1.layer.borderColor = UIColor.clearColor().CGColor
        imageBtn2.layer.borderWidth = 5
        imageBtn2.layer.borderColor = UIColor.lightGrayColor().CGColor
        ImageBtn3.layer.borderWidth = 5
        ImageBtn3.layer.borderColor = UIColor.clearColor().CGColor
        ImageBtn4.layer.borderWidth = 5
        ImageBtn4.layer.borderColor = UIColor.clearColor().CGColor
        
        self.parseCallToPostAnswer(sender as! UIButton)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // this methods post an answer to current question
    func parseCallToPostAnswer(sender1: UIButton)
    {
        if (Reachability.isConnectedToNetwork()){
            self.appDelegateObj.showIndicatorView()
            
            let compQuery = PFQuery(className:"AnswerNew")
            compQuery.whereKey("userId", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
            compQuery.whereKey("QuestionID", equalTo:self.GlobalquestionObjectITVC.objectId!)
            compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
                if(error == nil){
                    if results?.count == 0{ // insert a new row for the answer for question
                        var post:PFObject!
                        post  = PFObject(className: "AnswerNew")
                        if sender1.tag == 11{
                            post["SelectedOption"] = 1
                        }
                        else if sender1.tag == 12{
                            post["SelectedOption"] = 2
                        }
                        else if sender1.tag == 13{
                            post["SelectedOption"] = 3
                        }
                        else if sender1.tag == 14{
                            post["SelectedOption"] = 4
                        }
                        else{
                            post["SelectedOption"] = sender1.tag
                            
                        }
                        post["userId"] = self.appDelegateObj.loggedInUser?.objectId
                        post["QuestionID"] = self.GlobalquestionObjectITVC.objectId
                        post.saveInBackgroundWithBlock{
                            (success:Bool, error:NSError?)-> Void in
                            if success == true{
                                print(" answer submitted successfully")
                            }
                            self.appDelegateObj.hideIndicatorView()
                            
                        }
                    }
                    else{//update
                        
                        let objects = results as? [PFObject]
                        let obj:PFObject = objects![0]
                        
                        let query = PFQuery(className:"AnswerNew")
                        query.getObjectInBackgroundWithId(obj.objectId!) {
                            (object, error) -> Void in
                            if error != nil {
                            } else {
                                if let object = object {
                                    if sender1.tag == 11{
                                        object["SelectedOption"] = 1
                                    }
                                    else if sender1.tag == 12{
                                        object["SelectedOption"] = 2
                                    }
                                    else if sender1.tag == 13{
                                        object["SelectedOption"] = 3
                                    }
                                    else if sender1.tag == 14{
                                        object["SelectedOption"] = 4
                                    }
                                    else{
                                        object["SelectedOption"] = sender1.tag
                                        
                                    }
                                }
                                object!.saveInBackground()
                            }
                        }
                        self.appDelegateObj.hideIndicatorView()
                        
                    }
                    
                }
                
            })

        }
        else{
            let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
            alertView.show()
        }
        
    
            
            
        
    }
    // this method is checks all ansers for the current question
    func checkAnswersforAllUsers(){
        let compQuery = PFQuery(className:"AnswerNew")
        compQuery.whereKey("QuestionID", equalTo:self.GlobalquestionObjectITVC.objectId!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                var intone:Int = 0
                var intTwo:Int = 0
                var intThree:Int = 0
                var intFour:Int = 0
                if results?.count != 0{
                    
                    if let settObjects:NSArray = results {
                        for sett in settObjects {
                            // Use staff as!a standard PFObject now. e.g.
                            let que:PFObject = sett as! PFObject
                            
                           
                           
                            if que.objectForKey("SelectedOption") as! Int == 1{
                                intone++;
                            }
                            if que.objectForKey("SelectedOption") as! Int == 2{
                                intTwo++
                            }
                            if que.objectForKey("SelectedOption") as! Int == 3{
                                intThree++
                            }
                            if que.objectForKey("SelectedOption") as! Int == 4{
                                intFour++
                            }
                            
                           
                            
                            
                        }
                        self.lblOptionOneCount.text = "Opt 1: "+"\(intone)"
                        self.lblOptionTwoCount.text = "Opt 2: "+"\(intTwo)"
                        self.lblOptionThreeCount.text = "Opt 3: "+"\(intThree)"
                        self.lblOptionFourCount.text = "Opt 4: "+"\(intFour)"
                    }
                }
                else{
                    
                    self.lblOptionOneCount.text = "Opt 1: "+"\(intone)"
                    self.lblOptionTwoCount.text = "Opt 2: "+"\(intTwo)"
                    self.lblOptionThreeCount.text = "Opt 3: "+"\(intThree)"
                    self.lblOptionFourCount.text = "Opt 4: "+"\(intFour)"
                }
                
                // if question is expired then green border for an answer which is maximim answer
                if self.isQuestionExpired{
                    if self.GlobalquestionObjectITVC.isType == 1{ // text question

                        if (intone >= intTwo && intone >= intThree && intone >= intFour ){
                            self.option1.backgroundColor = UIColor.blueColor()

                        }
                        if (intTwo >= intThree && intTwo >= intFour && intTwo >= intone ){
                            self.option2.backgroundColor = UIColor.blueColor()
                        }
                        if (intThree >= intFour && intThree >= intone && intThree >= intTwo ){
                            self.option3.backgroundColor = UIColor.blueColor()
                        }
                        if (intFour >= intone && intFour >= intTwo && intFour >= intThree ){
                            self.option4.backgroundColor = UIColor.blueColor()
                        }
                        
                        
                        
                    }
                    if self.GlobalquestionObjectITVC.isType == 2{ // image question
                        if (intone >= intTwo && intone >= intThree && intone >= intFour ){
                            self.imageBtn1.layer.borderWidth = 5
                            self.imageBtn1.layer.borderColor = UIColor.blueColor().CGColor
                        }
                        if (intTwo >= intThree && intTwo >= intFour && intTwo >= intone ){
                            self.imageBtn2.layer.borderWidth = 5
                            self.imageBtn2.layer.borderColor = UIColor.blueColor().CGColor
                        }
                        if (intThree >= intFour && intThree >= intone && intThree >= intTwo ){
                            self.ImageBtn3.layer.borderWidth = 5
                            self.ImageBtn3.layer.borderColor = UIColor.blueColor().CGColor
                        }
                        if (intFour >= intone && intFour >= intTwo && intFour >= intThree ){
                            self.ImageBtn4.layer.borderWidth = 5
                            self.ImageBtn4.layer.borderColor = UIColor.blueColor().CGColor
                        }
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                
            }
            
        })
    }
    
    
    func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        return String(format: "%02dh:%02dm:%02ds", hours, minutes, seconds)
    }
    
    
    //rounded button to show elapsed timer
    @IBAction func btnTimerClicked(sender: UIButton) {
        //print("DATE COMPARISON")
        let elapsedTime = self.globalDate.timeIntervalSinceDate(NSDate())
       // print(elapsedTime)
        print("Elapsed time to expire question:\(self.timeFormatted(Int(elapsedTime)))")
        self.lblTimerElapsed.text = self.timeFormatted(Int(elapsedTime))
        
        UIView.animateWithDuration(1.0, delay: 0, options: .CurveEaseOut, animations: {
           self.lblTimerElapsed.alpha = 1.0
            }, completion: { finished in
                UIView.animateWithDuration(1.0, delay: 0, options: .CurveEaseOut, animations: {
                    self.lblTimerElapsed.alpha = 0.0
                    }, completion: { finished in
                })
        })
        
        
        
    }
}


