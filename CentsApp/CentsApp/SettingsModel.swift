//
//  SettingsModel.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 1/5/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//

import UIKit

class SettingsModel: NSObject {
    var objectId:NSString?
    var userId:NSString?
    var redExpiryTime:NSNumber?
    var questionInMyQuestionVisibility:NSNumber?
    var questionInInboxVisibility:NSNumber?
    var questionExpiryTimer:NSNumber?
   
}
