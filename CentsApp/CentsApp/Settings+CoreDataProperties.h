//
//  Settings+CoreDataProperties.h
//  CentsApp
//
//  Created by Sagar Jadhav on 1/25/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Settings.h"

NS_ASSUME_NONNULL_BEGIN

@interface Settings (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *parseObjectId;
@property (nullable, nonatomic, retain) NSNumber *questionExpiryTime;
@property (nullable, nonatomic, retain) NSNumber *redExpiryTime;
@property (nullable, nonatomic, retain) NSNumber *beforeRedTime;
@property (nullable, nonatomic, retain) NSNumber *afterRedTime;
@property (nullable, nonatomic, retain) NSNumber *questionInMyQuestionsVisibility;
@property (nullable, nonatomic, retain) NSNumber *questionInInboxVisibility;
@property (nullable, nonatomic, retain) NSNumber *questionInExploreVisibility;
@property (nullable, nonatomic, retain) NSNumber *questionExpiryTimer;

@end

NS_ASSUME_NONNULL_END
