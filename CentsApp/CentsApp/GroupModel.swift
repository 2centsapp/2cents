//
//  GroupModel.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 1/6/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//

import UIKit

class GroupModel: NSObject {
    var objectId:NSString?
    var owner:NSString?
    var groupName:NSString?
    var members:NSArray?
}
