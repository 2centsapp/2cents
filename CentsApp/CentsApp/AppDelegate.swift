//
//  AppDelegate.swift
//  CentsApp
//
//  Created by Mahesh on 10/30/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import Parse


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var loggedInUser:PFUser?
    var settingObject:SettingsModel!
    
    var viewProgressIndicator: UIView!
    var progressIndicator: UIActivityIndicatorView!
    
    //MARK: LifeCycle Methods
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Parse.setApplicationId("2PRWg5IYNj1iGsCZuzEb2z7ITUE7Vnf7FKCe4chp",
            clientKey: "jO21R7e5C1WFl7Ps587CCg3s2ZVFImzUo7maouJy")
        

        let userDefaults = NSUserDefaults.standardUserDefaults()
        let username:AnyObject? = userDefaults.valueForKey("UserName")
        
        
        if username == nil
        {
            print("First time logooin")
            self.firstTimeLogin();
        }else{
            print("Username available in defaults so skipping fist time login")
            self.doLogin()
        }

        return true
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func countMultiples(ofNumber number:Int, inNumbers numbers: [Int])
    {
        
    }
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    func sessionStateChanged(session : FBSession, state : FBSessionState, error : NSError?)
    {
        // If the session was opened successfully
        if error == nil
        {
            if  state == FBSessionState.Open
            {
                print("Session Opened")
                NSNotificationCenter.defaultCenter().postNotificationName("SessionStateChangeNotification", object: nil)
                
            }
            // If the session closed
            if state == FBSessionState.Closed
            {
                print("Closed")
            }
        }
        
    }
    
    func applicationDidBecomeActive(application: UIApplication)
    {
        
        if FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded
        {
            
            // If there's one, just open the session silently, without showing the user the login UI
            FBSession.openActiveSessionWithReadPermissions(nil, allowLoginUI: false, completionHandler: {
                (session:FBSession!, state:FBSessionState, error:NSError!) in
                self.sessionStateChanged(session, state: state, error: error)
            })
        }
//        FBAppCall.handleDidBecomeActive()
        
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        return FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
    }
    
    
    
    
    
    //    func openActiveSessionWithPermissions(permissions:NSArray?, allowLoginUI: Bool)
    //    {
    ////        FBSession.openActiveSessionWithReadPermissions(permissions, allowLoginUI: allowLoginUI, completionHandler: (session:FBSession(), status: FBSessionState(), error: NSError()){
    ////
    ////            })
    //
    //        FBSession.openActiveSessionWithReadPermissions(permissions, allowLoginUI: allowLoginUI, completionHandler: {
    //            (session, status, error) -> Void in
    //            self.sessionStateChanged(session, status: status, error: error)
    //        })
    //
    //
    //
    //    }
    
    
    //MARK: Login Methods
    func firstTimeLogin(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController") as? ViewController
        let navigationController = UINavigationController(rootViewController: rootViewController!)
        navigationController.navigationBarHidden = true // or not, your choice.
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window!.rootViewController = navigationController
        self.window!.makeKeyAndVisible()
        
    }
    func doLogin(){
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let username:String = userDefaults.valueForKey("UserName") as! String
        let password:String = userDefaults.valueForKey("Password") as! String
        
        
        PFUser.logInWithUsernameInBackground(username, password: password , block: { (user:PFUser?, error:NSError?) -> Void in
            if (error == nil){
                
                self.loggedInUser = user
                self.fetchSettingsFromParse()
                
            }else{
                self.firstTimeLogin()
            }
            
        })
        
        
    }
    //MARK: Settings Methods
    func fetchSettingsFromParse(){
        let compQuery = PFQuery(className:"Settings")
        compQuery.whereKey("userId", equalTo:(self.loggedInUser?.objectId)!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if results?.count != 0{
                    if let settObjects:NSArray = results {
                        for sett in settObjects {
                            // Use staff as!a standard PFObject now. e.g.
                            let que:PFObject = sett as! PFObject
                            
                            self.settingObject = SettingsModel()
                            self.settingObject.objectId = que.objectId
                            self.settingObject.userId = que.objectForKey("userId") as!String
                            self.settingObject.redExpiryTime = que.objectForKey("redExpiryTime") as!Int
                            self.settingObject.questionExpiryTimer = que.objectForKey("questionExpiryTimer") as!Int
                            self.settingObject.questionInInboxVisibility = que.objectForKey("questionInInboxVisibility") as!Int
                            self.settingObject.questionInMyQuestionVisibility = que.objectForKey("questionInMyQuestionsVisibility") as!Int
                        }
                        
                    }
                }
            }
            else{
                print("Error in retrieving \(error)")
            }
            
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeobj = mainStoryboard.instantiateViewControllerWithIdentifier("homeScreen") as? HomeViewController
            let navigationController = UINavigationController(rootViewController: homeobj!)
            navigationController.navigationBarHidden = true // or not, your choice.
            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
            self.window!.rootViewController = navigationController
            self.window!.makeKeyAndVisible()
            
        })
    }
    //MARK: UIActivityIndicatorView Methods
    func showIndicatorView(){
        viewProgressIndicator = UIView()
        progressIndicator = UIActivityIndicatorView()
        viewProgressIndicator.frame = self.window!.frame
        self.window?.addSubview(viewProgressIndicator)
        progressIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        progressIndicator.center = viewProgressIndicator.center
        progressIndicator.hidesWhenStopped = true
        progressIndicator.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        viewProgressIndicator.addSubview(progressIndicator)
        progressIndicator.startAnimating()
    }
    
    func hideIndicatorView(){
        
        viewProgressIndicator.removeFromSuperview()
        progressIndicator.stopAnimating()
    }
}

