//
//  QuestionCell.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 11/18/15.
//  Copyright (c) 2015 Sagar Jadhav. All rights reserved.
//

import UIKit
import Foundation

class QuestionCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
     @IBOutlet weak var viewWithTwoOptionsImage: UIView!
     @IBOutlet weak var viewWithThreeOptionsImage: UIView!
     @IBOutlet weak var viewWithFourOptionsImage: UIView!
     @IBOutlet weak var viewWithTextOptions: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
