//
//  Facebook.swift
//  CentsApp
//
//  Created by Mahesh on 10/30/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import Foundation



let FB = Facebook();

class Facebook {
    
    var fbSession:FBSession?;
    
    init(){
        self.fbSession = FBSession.activeSession();
    }
    
    func hasActiveSession() -> Bool{
        let fbsessionState = FBSession.activeSession().state;
        if ( fbsessionState == FBSessionState.Open
            || fbsessionState == FBSessionState.OpenTokenExtended ){
                self.fbSession = FBSession.activeSession();
                return true;
        }
        return false;
    }
    
    func login(callback: () -> Void){
        
        let permission = ["basic_info", "email", "user_work_history", "user_education_history", "user_location"];
        
        let activeSession = FBSession.activeSession();
        let fbsessionState = activeSession.state;
        var showLoginUI = true;
        
        if(fbsessionState == FBSessionState.CreatedTokenLoaded){
            showLoginUI = false;
        }
        
        if(fbsessionState != FBSessionState.Open
            && fbsessionState != FBSessionState.OpenTokenExtended){
                FBSession.openActiveSessionWithReadPermissions(
                    permission,
                    allowLoginUI: showLoginUI,
                    completionHandler: { (session:FBSession!, state:FBSessionState, error:NSError!) in
                        
                        if(error != nil){
                            print("Session Error: \(error)");
                        }
                        
                        self.fbSession = session;
                        
                        callback();
                        
                    }
                );
                return;
        }
        
        callback();
        
    }
    
    func logout(){
        self.fbSession?.closeAndClearTokenInformation();
        self.fbSession?.close();
    }
    
    func getInfo(){
        FBRequest.requestForMe()?.startWithCompletionHandler({(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) in
            
            if(error != nil){
                print("Error Getting ME: \(error)");
            }
            
            print("\(result)");
            
            
        });
    }
    
    func handleDidBecomeActive(){
        FBAppCall.handleDidBecomeActive();
    }
    
}
