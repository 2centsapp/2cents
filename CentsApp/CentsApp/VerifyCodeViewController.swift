//
//  VerifyCodeViewController.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 12/31/15.
//  Copyright © 2015 Rapidera. All rights reserved.
//

import UIKit
import Foundation
import Parse

class VerifyCodeViewController: UIViewController,UITextFieldDelegate{
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate
    var mobileNumber:NSString!
    @IBOutlet var btnVerifyCode: UIButton!
    @IBOutlet var txtFldVerifyCode: UITextField!
    @IBOutlet var txtFldCreatePassword: UITextField!
    var verificationCode:NSString! = ""
    var passwordExistsInUserData:NSString! = ""

    
    override func viewDidLoad()
    {
        self.getUserFromUserDataTable(mobileNumber)
    }
    override func viewDidDisappear(animated: Bool)
    {
    }
    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func btnVerifyCodeClicked(sender: UIButton) {
        
       
        if self.passwordExistsInUserData.isEqualToString(""){
            if (txtFldVerifyCode.text == self.verificationCode && (!txtFldCreatePassword.text!.isEmpty)){
                self.registerUserTable()
            }else{
                let myAlert = UIAlertController(title:"Alert", message:"Please create password", preferredStyle: UIAlertControllerStyle.Alert);
                let okAction =  UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                myAlert.addAction(okAction);
                self.presentViewController(myAlert, animated:true, completion:nil);
            }
        }else{
            if (txtFldVerifyCode.text == self.verificationCode){
                self.existingUserLogin(mobileNumber)
            }else{
                let myAlert = UIAlertController(title:"Alert", message:"Please enter correct code", preferredStyle: UIAlertControllerStyle.Alert);
                let okAction =  UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                myAlert.addAction(okAction);
                self.presentViewController(myAlert, animated:true, completion:nil);
            }
            
        }
        
       // let homeobj = self.storyboard?.instantiateViewControllerWithIdentifier("homeScreen") as! HomeViewController
       // self.navigationController?.pushViewController(homeobj, animated: false)
    }
    
    func getUserFromUserDataTable(usernumber:NSString){
        
        let compQuery = PFQuery(className:"UserData")
        compQuery.whereKey("phoneNumber", equalTo:usernumber)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if results?.count>0{
                    let objects = results as? [PFObject]
                    let obj:PFObject = objects![0]
                    print(obj.objectForKey("verificationCode") as! String);
                   self.verificationCode = obj.objectForKey("verificationCode") as! String
                    self.passwordExistsInUserData = obj.objectForKey("password") as! String

                    if self.passwordExistsInUserData.isEqualToString(""){
                        self.txtFldCreatePassword.hidden = false;
                    }else{
                        self.txtFldCreatePassword.hidden = true;
                    }
                    
                }
                
            }
            else{
                print("Error in retrieving \(error)")
            }
            
        })
    }
    
    func registerUserTable(){
        self.appDelegateObj.showIndicatorView()
        let newUser = PFUser()
        newUser.username = self.mobileNumber as String
        let string1 = self.mobileNumber as String;
        newUser.email = "temp"+string1+"@gmail.com"
        let phoneNumber:String? = self.mobileNumber as String
        newUser.password = self.txtFldCreatePassword.text
        let a:Int? = self.verificationCode.integerValue
        newUser.setValue(a, forKey: "verifyCode")
        newUser.setValue(phoneNumber, forKey: "phoneNumber")

        newUser.signUpInBackgroundWithBlock{ (succeeded:Bool?, error:NSError?) -> Void in
            if (error == nil){
                print("User registration successful")
                
                print(newUser.objectId);
                self.appDelegateObj.loggedInUser = newUser
                
                let userDefaults = NSUserDefaults.standardUserDefaults()
                userDefaults.setValue(newUser.username, forKey: "UserName")
                userDefaults.setValue(phoneNumber, forKey: "PhoneNumber")
                userDefaults.setValue(self.txtFldCreatePassword.text, forKey: "Password")
                userDefaults.synchronize()
                
                let homeobj = self.storyboard?.instantiateViewControllerWithIdentifier("homeScreen") as! HomeViewController
                self.navigationController?.pushViewController(homeobj, animated: false)
                
                let compQuery = PFQuery(className:"UserData")
                compQuery.whereKey("username", equalTo:phoneNumber!)
                compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
                    if results?.count != 0{
                    let objects = results as? [PFObject]
                    let obj:PFObject = objects![0]
                    
                    let query = PFQuery(className:"UserData")
                    query.getObjectInBackgroundWithId(obj.objectId!) {
                        (object, error) -> Void in
                        if error != nil {
                        } else {
                            if let object = object {
                                object["password"] = self.txtFldCreatePassword.text as String!
                            }
                            object!.saveInBackground()
                        }
                    }
                    }
                    
                })
                
                
            }else{
               
                
                
            }
            self.appDelegateObj.hideIndicatorView()

        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    func existingUserLogin(usernumber:NSString){
        
        var passwordFromUserData:String!
        let compQuery = PFQuery(className:"UserData")
        compQuery.whereKey("username", equalTo:usernumber)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if results?.count>0{
                    let objects = results as? [PFObject]
                    let obj:PFObject = objects![0]
                    print(obj.objectForKey("password") as! String);
                    passwordFromUserData = obj.objectForKey("password") as! String
                    self.doLogin(passwordFromUserData)
                }
                
            }
            else{
                print("Error in retrieving \(error)")
            }
            
        })
            
            
            
}

    func  doLogin(passwordFromUserData:String){
        
        self.appDelegateObj.showIndicatorView()

        //already exists
        let newUser = PFUser()
        newUser.username = self.mobileNumber as String
        let string1 = self.mobileNumber as String;
        newUser.email = "temp"+string1+"@gmail.com"
        let phoneNumber:String? = self.mobileNumber as String
        newUser.password = passwordFromUserData as String
        let a:Int? = self.verificationCode.integerValue
        newUser.setValue(a, forKey: "verifyCode")
        newUser.setValue(phoneNumber, forKey: "phoneNumber")
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(newUser.username, forKey: "UserName")
        userDefaults.setValue(phoneNumber, forKey: "PhoneNumber")
        userDefaults.setValue(passwordFromUserData, forKey: "Password")
        userDefaults.synchronize()
        
        let username:String = userDefaults.valueForKey("UserName") as! String
        let password:String = userDefaults.valueForKey("Password") as! String
        
        PFUser.logInWithUsernameInBackground(username, password: password , block: { (user:PFUser?, error:NSError?) -> Void in
            if (error == nil){
                
                self.appDelegateObj.loggedInUser = user
                let compQuery = PFQuery(className:"Settings")
                compQuery.whereKey("userId", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
                compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
                    if(error == nil){
                        if results?.count != 0{
                            if let settObjects:NSArray = results {
                                for sett in settObjects {
                                    // Use staff as!a standard PFObject now. e.g.
                                    let que:PFObject = sett as! PFObject
                                    
                                    self.appDelegateObj.settingObject = SettingsModel()
                                    self.appDelegateObj.settingObject.objectId = que.objectId
                                    self.appDelegateObj.settingObject.userId = que.objectForKey("userId") as!String
                                    self.appDelegateObj.settingObject.redExpiryTime = que.objectForKey("redExpiryTime") as!Int
                                    self.appDelegateObj.settingObject.questionExpiryTimer = que.objectForKey("questionExpiryTimer") as!Int
                                    self.appDelegateObj.settingObject.questionInInboxVisibility = que.objectForKey("questionInInboxVisibility") as!Int
                                    self.appDelegateObj.settingObject.questionInMyQuestionVisibility = que.objectForKey("questionInMyQuestionsVisibility") as!Int
                                }
                                
                            }
                        }
                        
                        let homeobj = self.storyboard?.instantiateViewControllerWithIdentifier("homeScreen") as! HomeViewController
                        self.navigationController?.pushViewController(homeobj, animated: false)
                        
                        
                    }
                    else{
                        print("Error in retrieving \(error)")
                    }

                 })
            }else{
            }
            self.appDelegateObj.hideIndicatorView()

        })
    }
    
    
    

}
