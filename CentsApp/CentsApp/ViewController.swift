//
//  ViewController.swift
//  CentsApp
//
//  Created by Mahesh on 10/30/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import Parse
class ViewController: UIViewController, FBLoginViewDelegate,UITextFieldDelegate {
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var txtReferalCode: UITextField!
    let singleton = Singleton.sharedInstance
    //MARK: LifeCycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleFBSessionStateChangeWithNotification:", name: "SessionStateChangeNotification", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK: Button Action Methods
    @IBAction func facebookLogin()
    {
     
        
        if (FBSession.activeSession().state == FBSessionState.Open || FBSession.activeSession().state == FBSessionState.OpenTokenExtended)
        {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            print(userDefaults.objectForKey("Facebook_Dictionary"))
            let dict:NSDictionary = userDefaults.objectForKey("Facebook_Dictionary") as! NSDictionary

          
           
            let total = dict.allKeys.count
            if total == 0{
                FBSession.activeSession().closeAndClearTokenInformation()
                FBSession.openActiveSessionWithReadPermissions(["public_profile", "email"], allowLoginUI: true, completionHandler: {
                    (session:FBSession!, state:FBSessionState, error:NSError!) in
                    
                    self.appDelegateObj.sessionStateChanged(session, state: state, error: error)
                })
            }else{
                
//                let dateFormatter = NSDateFormatter()
//                let dateString = dateFormatter.stringFromDate(FBSession.activeSession().expirationDate)
//                
//                let innerDict:[String:String] = ["access_token":FBSession.activeSession().accessToken,"expiration_date": dateString ,"id":dict.valueForKey("id")as! String]
//                 let outerDict:[String:NSDictionary] = ["facebook":innerDict]
                
                FBSession.activeSession().description
                
                let email:NSString = dict.valueForKey("email") as! NSString
                
                self.checkEmailAlreadyExists(email);
                
            }
        }
        else
        {
            FBSession.openActiveSessionWithReadPermissions(["public_profile", "email"], allowLoginUI: true, completionHandler: {
                (session:FBSession!, state:FBSessionState, error:NSError!) in
                
                self.appDelegateObj.sessionStateChanged(session, state: state, error: error)
            })
        }
    }
    
  
    @IBAction func btnSendVerification(sender: UIButton) {
        
        if (isValidate()){
            let mutabledict = ["number":self.txtMobileNumber.text as String!]
            
            PFCloud.callFunctionInBackground("verifyNumber", withParameters: mutabledict as [NSObject : AnyObject]) { (result: AnyObject?, error: NSError?) in
                print("-----verifyNumber ----- method -----")
                // print(Zones)
                
                let verifyCode = self.storyboard?.instantiateViewControllerWithIdentifier("verifyCodeScreen") as! VerifyCodeViewController
                verifyCode.mobileNumber = self.txtMobileNumber.text
                self.navigationController?.pushViewController(verifyCode, animated: false)
            }

        }
        
        
        
        //  let verifyCode = self.storyboard?.instantiateViewControllerWithIdentifier("verifyCodeScreen") as! VerifyCodeViewController
        //  verifyCode.mobileNumber = self.txtMobileNumber.text
        //  self.navigationController?.pushViewController(verifyCode, animated: false)
    }
    
    func handleFBSessionStateChangeWithNotification(notification: NSNotification)
    {
//        me?fields=id,name,email,gender,birthday,albums{photos{images}}
        
        FBRequestConnection.startWithGraphPath("me?fields=id,name,email,gender,birthday,albums{photos{images}},hometown", completionHandler: {
            (connection:FBRequestConnection!, result:AnyObject! , error:NSError!) in
            
            if error == nil
            {
                print("the data captured \(result)")
                let dict:NSDictionary = result as! NSDictionary
                let email:NSString = dict.valueForKey("email") as! NSString
                
                 let userDefaults = NSUserDefaults.standardUserDefaults()
                
                userDefaults.setObject(result, forKey: "Facebook_Dictionary")
                userDefaults.synchronize()
                
                
                self.checkEmailAlreadyExists(email);
                
                
            }
            else
            {
                print("the error occured \(error.description)")
            }
            
        })
        
    }
    
    
    
    //MARK: Other Methods
    func checkEmailAlreadyExists(email:NSString) {
        let query:PFQuery = PFUser.query()!
        query.findObjectsInBackgroundWithBlock { (result: [AnyObject]?, error:NSError?) -> Void in
            print("RESULT: \(result)")
            
            let userArray:NSArray = result! as NSArray
            print(userArray.count)
            
            var isFound = false;
            var user:PFUser
            var emailId:String = ""
            for var i = 0; i < userArray.count; i++
            {
                user = userArray.objectAtIndex(i) as! PFUser
                print(user.email);
                print(user.password);
                if user.email == email{
                  //  print("User already exists in table");
                    isFound = true
                    emailId = user.email! as String
                    break
                }else{
                  //  print("Create a new user");
                }
            }
            
            
            if isFound
            {
                print("User already exists in table....Update user table");
                
                
                let compQuery = PFQuery(className:"UserData")
                compQuery.whereKey("username", equalTo:email)
                compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
                    if(error == nil){
                        if result?.count>0{
                            let objects = results as? [PFObject]
                            let obj:PFObject = objects![0]
                            print(obj.objectForKey("password") as! String);
                            PFUser.logInWithUsernameInBackground(emailId, password: obj.valueForKey("password") as! String, block: { (user:PFUser?, error:NSError?) -> Void in
                                if (error == nil){
                                    
                                    let homeobj = self.storyboard?.instantiateViewControllerWithIdentifier("homeScreen") as! HomeViewController
                                    self.navigationController?.pushViewController(homeobj, animated: false)
                                    
                                }else{
                                    print("SOME ERRORR")
                                    
                                    let myAlert = UIAlertController(title:"Alert", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert);
                                    
                                    let okAction =  UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                    
                                    myAlert.addAction(okAction);
                                    self.presentViewController(myAlert, animated:true, completion:nil);
                                    
                                }
                            })
                        }
                        
                    }
                    else{
                        print("Error in retrieving \(error)")
                    }
                    
                })
                
                
                
                
            }else{
                print("Create a new user");
                self.createNewUserWithFirstTimeFacebookLogin()
                
                
            }
        }
    }
    
    
    //MARK: Facebook Methods
    func createNewUserWithFirstTimeFacebookLogin(){
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile","email"], block: { (user:PFUser?, error:NSError?) -> Void in
            
            if(error != nil)
            {
                //Display an alert message
                let myAlert = UIAlertController(title:"Alert", message:error?.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert);
                
                let okAction =  UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                
                myAlert.addAction(okAction);
                self.presentViewController(myAlert, animated:true, completion:nil);
                
                
                return
            }
            
            if user == nil {
                NSLog("Uh oh. The user cancelled the Facebook login.")
            }
            else if (user!.isNew)
            {
                NSLog("User signup in through Facebook! \(user)")
                
                let homeobj = self.storyboard?.instantiateViewControllerWithIdentifier("homeScreen") as! HomeViewController
                self.navigationController?.pushViewController(homeobj, animated: false)
            }
            else
            {
                NSLog("User logged in through Facebook! \(user)")
                
            }
        })
    }
    
    
    //MARK: UITextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    
    func isValidate()->Bool{
        
        if (txtMobileNumber.text?.characters.count == 0) {
            let alert = UIAlertController(title: "Alert", message:"Pleas enter phone number.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                //print("completion block")
            })
            return false
        }
        else if ((!(singleton.isPhoneNumberValid(self.txtMobileNumber.text!))) || (self.txtMobileNumber.text?.characters.first != "+")){
            let alert = UIAlertController(title: "Alert", message:"Please enter valid phone number", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
               // print("completion block")
            })
            return false
        }

        return true
    }
}

