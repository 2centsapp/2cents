//
//  Questions+CoreDataProperties.m
//  CentsApp
//
//  Created by Sagar Jadhav on 1/21/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Questions+CoreDataProperties.h"

@implementation Questions (CoreDataProperties)

@dynamic parseObjectId;
@dynamic question;
@dynamic isType;
@dynamic expiryTime;
@dynamic questionDate;
@dynamic owner;
@dynamic optionCount;
@dynamic expiryDate;
@dynamic textOptionOne;
@dynamic textOptionTwo;
@dynamic textOptionThree;
@dynamic textOptionFour;
@dynamic imageOptionOne;
@dynamic imageOptionTwo;
@dynamic imageOptionThree;
@dynamic imageOptionFour;
@dynamic isExpired;

@end
