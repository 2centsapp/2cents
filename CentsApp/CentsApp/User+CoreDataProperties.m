//
//  User+CoreDataProperties.m
//  CentsApp
//
//  Created by Sagar Jadhav on 1/18/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic userName;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic phoneNumber;

@end
