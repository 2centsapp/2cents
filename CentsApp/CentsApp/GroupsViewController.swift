//
//  GroupsViewController.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 1/6/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//

import UIKit
import Parse
class GroupsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet var tblView: UITableView!
    var items: [GroupModel]! = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.appDelegateObj.showIndicatorView()

        let compQuery = PFQuery(className:"ContactGroups")
        compQuery.whereKey("owner", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if let questionsObjects:NSArray = results {
                    for question in questionsObjects {
                        // Use staff as!a standard PFObject now. e.g.
                        let que:PFObject = question as! PFObject
                        
                        let questionObject = GroupModel()
                        questionObject.objectId = que.objectId
                        questionObject.owner = que.objectForKey("owner") as!String
                        questionObject.groupName = que.objectForKey("groupName") as!String
                        //questionObject.members = que.objectForKey("memberIds") as!NSArray
                        
                        
                        self.items.append(questionObject)
                        // self.tableViewMyQuestions.reloadData()
                    }
                    print(self.items);
                    self.appDelegateObj.hideIndicatorView()

                    self.tblView.reloadData()
                }
            }
            else{
                print("Error in retrieving \(error)")
            }
            
        })
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = self.tblView.dequeueReusableCellWithIdentifier("cellGroups")! as UITableViewCell
        
        let questionObject:GroupModel! = self.items[indexPath.row]
        
        let name: String = questionObject.groupName as!String
        cell.textLabel?.text = name
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    @IBAction func btnBackClicked(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(false)
    }
}
