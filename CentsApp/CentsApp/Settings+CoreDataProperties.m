//
//  Settings+CoreDataProperties.m
//  CentsApp
//
//  Created by Sagar Jadhav on 1/25/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Settings+CoreDataProperties.h"

@implementation Settings (CoreDataProperties)

@dynamic parseObjectId;
@dynamic questionExpiryTime;
@dynamic redExpiryTime;
@dynamic beforeRedTime;
@dynamic afterRedTime;
@dynamic questionInMyQuestionsVisibility;
@dynamic questionInInboxVisibility;
@dynamic questionInExploreVisibility;
@dynamic questionExpiryTimer;

@end
