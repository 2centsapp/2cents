//
//  Singleton.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 1/22/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//

import UIKit
import Foundation

class Singleton: NSObject {

    let validPhoneNumberCharacters:String = "0123456789+"
    let validNumberCharacters:String = "0123456789"
    let validAlphaNumberCharacters:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
    class var sharedInstance: Singleton {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: Singleton? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = Singleton()
        }
        return Static.instance!
    }
    
    
    
    //Mark check Empty String
    func isStringEmpty(string:NSString)->Bool{
        if ( string.isKindOfClass(NSNull) || string == "" || string == "<null>" || NSString(format: "%@",string).length == 0){
           return true
        }
        return false
    }
    func validateEmailWithString(email:NSString)->Bool{
        let emailRegex:NSString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(email)
    }
    func isPhoneNumberValid(string:NSString)->Bool{
        let set:NSCharacterSet = NSCharacterSet(charactersInString:validPhoneNumberCharacters).invertedSet
    
        if (string.rangeOfCharacterFromSet(set).location != NSNotFound){
            return false
        }
        else{
            return true
        }
    
    }
   
    func isNumberValid(string:NSString)->Bool{
        let set:NSCharacterSet = NSCharacterSet(charactersInString:validNumberCharacters).invertedSet
        
        if (string.rangeOfCharacterFromSet(set).location != NSNotFound){
            return false
        }
        else{
            return true
        }
        
    }
   
    func isAphaNumericValid(string:NSString)->Bool{
        let set:NSCharacterSet = NSCharacterSet(charactersInString:validAlphaNumberCharacters).invertedSet
        
        if (string.rangeOfCharacterFromSet(set).location != NSNotFound){
            return false
        }
        else{
            return true
        }
        
    }
    
}
