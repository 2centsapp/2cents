//
//  CoreDataManager.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 1/18/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import Parse
class CoreDataManager: NSObject {
    var managedObjectContextObj:NSManagedObjectContext!
    var managedObjectModelObj:NSManagedObjectModel!
    var persistentStoreCoordinatorObj:NSPersistentStoreCoordinator!
    var singleton:Singleton!
    var context:NSManagedObjectContext!
    //create shared instance WAY 1
    //static let sharedInstance: CoreDataManager = CoreDataManager()
    
    //create shared instance WAY 2
    /*class var sharedInstance: CoreDataManager {
        struct Static {
            static let instance: CoreDataManager = CoreDataManager()
        }
        return Static.instance
    }*/
    
    //create shared instance WAY 3
    class var sharedInstance: CoreDataManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: CoreDataManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = CoreDataManager()
        }
        return Static.instance!
    }
    
    
    override init() {
        super.init()
        self.context = managedObjectContext()
    }
    
    //MARK: CoreData Methods
    func managedObjectContext()->NSManagedObjectContext{
        if self.managedObjectContextObj != nil{
            return self.managedObjectContextObj
        }
        let coordinator:NSPersistentStoreCoordinator! = self.persistentStoreCoordinator()
    
        if coordinator != nil{
             self.managedObjectContextObj = NSManagedObjectContext()
            self.managedObjectContextObj.persistentStoreCoordinator = coordinator
        }
    
        return self.managedObjectContextObj
    
    
    }
    func persistentStoreCoordinator()->NSPersistentStoreCoordinator{
        // while magrarting your app need send options
       /* var dict:NSDictionary = [NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true]
        //and repalce below line
        try self.persistentStoreCoordinatorObj!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: dict as [NSObject : AnyObject])*/
        
        if self.persistentStoreCoordinatorObj != nil{
            return self.self.persistentStoreCoordinatorObj
        }
        let storeURL:NSURL = self.applicationDocumentsDirectory().URLByAppendingPathComponent("CoreDataModel.sqlite")
        print("sqlite path:")
        print(storeURL)
        self.persistentStoreCoordinatorObj = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel())
        do {
            try self.persistentStoreCoordinatorObj!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            print("Failed to load store")
            abort();
        }
        return self.persistentStoreCoordinatorObj;
        
        
    }
    
    func applicationDocumentsDirectory()->NSURL{
         let paths = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let documentsURL = paths[0] 
        return documentsURL
    }
    
    func managedObjectModel()->NSManagedObjectModel{
        if self.managedObjectModelObj != nil{
            return self.managedObjectModelObj
        }
        var modelURL:NSURL
        modelURL = NSBundle.mainBundle().URLForResource("CoreDataModel", withExtension: "momd")!
        self.managedObjectModelObj = NSManagedObjectModel(contentsOfURL: modelURL)
        return self.managedObjectModelObj
    }
    
    //MARK: User Methods
    func saveSelfUserData(userdata:PFUser){
        let prd = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context)
        print(userdata.valueForKey("firstName"))
        prd.setValue(userdata.valueForKey("firstName"), forKey: "firstName")
        prd.setValue(userdata.valueForKey("lastName"), forKey: "lastName")
        prd.setValue(userdata.email, forKey: "email")
        prd.setValue(userdata.username, forKey: "userName")
        prd.setValue(userdata.username, forKey: "phoneNumber")
        
        do {
            try self.context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        
    }
    
    func updateSelfUserData(userdata:PFUser){
        let request = NSFetchRequest(entityName: "User")
        let resultPredicate = NSPredicate(format: "phoneNumber ==  %@", userdata.username!)
        request.predicate = resultPredicate
        var array:NSArray = []
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        if array.count == 1{
            let obj:NSManagedObject = array.objectAtIndex(0) as! NSManagedObject
            obj.setValue(userdata.valueForKey("firstName"), forKey: "firstName")
            obj.setValue(userdata.valueForKey("lastName"), forKey: "lastName")
            obj.setValue(userdata.email, forKey: "email")
            obj.setValue(userdata.username, forKey: "userName")
            obj.setValue(userdata.username, forKey: "phoneNumber")
        }
        
        do {
            try self.context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        
    }
    
    
    func getSelfUSerData(phoneNumber:String)->NSDictionary{
        var array:NSArray = []
        let request = NSFetchRequest(entityName: "User")
        let resultPredicate = NSPredicate(format: "phoneNumber ==  %@", phoneNumber)
        request.predicate = resultPredicate
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        let dict:NSMutableDictionary = NSMutableDictionary()
        if array.count == 1{
            let obj:NSManagedObject = array.objectAtIndex(0) as! NSManagedObject
            dict.setValue(obj.valueForKey("phoneNumber"), forKey: "phoneNumber")
            dict.setValue(obj.valueForKey("firstName"), forKey: "firstName")
            dict.setValue(obj.valueForKey("lastName"), forKey: "lastName")
            dict.setValue(obj.valueForKey("email"), forKey: "email")
            dict.setValue(obj.valueForKey("userName"), forKey: "userName")
        }

        
        return dict;
    }

    func getAllUSerData()->NSMutableArray{
        var array:NSArray = []
        let request = NSFetchRequest(entityName: "User")
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        let arr:NSMutableArray = NSMutableArray()
        if array.count > 0 {
            for sett in array {
                let dict:NSMutableDictionary = NSMutableDictionary()
                let obj:NSManagedObject = sett as! NSManagedObject
                dict.setValue(obj.valueForKey("phoneNumber"), forKey: "phoneNumber")
                dict.setValue(obj.valueForKey("firstName"), forKey: "firstName")
                dict.setValue(obj.valueForKey("lastName"), forKey: "lastName")
                dict.setValue(obj.valueForKey("email"), forKey: "email")
                dict.setValue(obj.valueForKey("userName"), forKey: "userName")
                arr.addObject(dict)
            }
            
        }
        
        
        return arr;
    }
    
    //MARK: Questions Methods
    
    func saveQuestionsArrayIntoDatabase(qObjects:NSArray){
        // first check question is available or not in database and then insert it.
        for question in qObjects {
            let que = question as! QuestionModel
            
            if (self.isQuestionAlreadyExistsInDatabaseWithId(que.objectId!)){
                //skip
            }else{
                //insert a question into database
                self.saveQuestionObjIntoDatabase(que)
            }
            
        }
        
        
    }
    
    func isQuestionAlreadyExistsInDatabaseWithId(objId:NSString)->Bool{
        var array:NSArray = []
        var isExists:Bool = false
        let request = NSFetchRequest(entityName: "Questions")
        let resultPredicate = NSPredicate(format: "parseObjectId ==  %@", objId)
        request.predicate = resultPredicate
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        if (array.count > 0) {
            isExists = true;
        }
        else{
            isExists = false;
        }
        return isExists;
    }
    
    func saveQuestionObjIntoDatabase(qObject:QuestionModel){
        let prd = NSEntityDescription.insertNewObjectForEntityForName("Questions", inManagedObjectContext: context)
        
        if (!(self.isStringEmpty(qObject.objectId!))){
            prd.setValue(qObject.objectId, forKey: "parseObjectId")
        }
        if (!(self.isStringEmpty(qObject.question!))){
            prd.setValue(qObject.question, forKey: "question")
        }
        if (!(self.isStringEmpty(qObject.questionDate!))){
            prd.setValue(qObject.questionDate, forKey: "questionDate")
        }
        if (!(self.isStringEmpty(qObject.owner!))){
            prd.setValue(qObject.owner, forKey: "owner")
        }
        if (!(self.isStringEmpty(qObject.expiryDate!))){
            prd.setValue(qObject.expiryDate, forKey: "expiryDate")
        }
        if (!(self.isStringEmpty(qObject.textOptionOne!))){
            prd.setValue(qObject.textOptionOne, forKey: "textOptionOne")
        }
        if (!(self.isStringEmpty(qObject.textOptionTwo!))){
            prd.setValue(qObject.textOptionTwo, forKey: "textOptionTwo")
        }
        if (!(self.isStringEmpty(qObject.textOptionThree!))){
            prd.setValue(qObject.textOptionThree, forKey: "textOptionThree")
        }
        if (!(self.isStringEmpty(qObject.textOptionFour!))){
            prd.setValue(qObject.textOptionFour, forKey: "textOptionFour")
        }
//        if (qObject.imageOptionOne != nil){
//            prd.setValue(qObject.imageOptionOne, forKey: "imageOptionOne")
//        }
//        if (qObject.imageOptionTwo != nil){
//            prd.setValue(qObject.imageOptionTwo, forKey: "imageOptionTwo")
//        }
//        if (qObject.imageOptionThree != nil){
//            prd.setValue(qObject.imageOptionThree, forKey: "imageOptionThree")
//        }
//        if (qObject.imageOptionFour != nil){
//            prd.setValue(qObject.imageOptionFour, forKey: "imageOptionFour")
//        }
        
        
        prd.setValue(qObject.isType, forKey: "isType")
        prd.setValue(qObject.expiryTime, forKey: "expiryTime")
        prd.setValue(qObject.optionCount, forKey: "optionCount")
        //prd.setValue(qObject.isExpired, forKey: "isExpired")
        
        do {
            try self.context.save()
        } catch let error as NSError  {
            print("Could not save question object: \(error), \(error.userInfo)")
        }
        
        
        
        
    }
    

    func getQuestionsDataFromDatabase(allQuestions:Bool,owner:NSString)->[QuestionModel]{
        var array:NSArray = []
        let request = NSFetchRequest(entityName: "Questions")
        if (!allQuestions){
            let resultPredicate = NSPredicate(format: "owner ==  %@", owner)
            request.predicate = resultPredicate
        }
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        
        
        var arr: [QuestionModel]! = []
        if array.count > 0 {
            for questions in array {
                let qObj:QuestionModel = QuestionModel()
                let obj = questions as! NSManagedObject
                
                if (!self.isStringEmpty(obj.valueForKey("parseObjectId") as! NSString)) {
                    qObj.objectId = obj.valueForKey("parseObjectId") as? NSString;
                }
                else{
                    qObj.objectId = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("question") as! NSString)) {
                    qObj.question = obj.valueForKey("question") as? NSString;
                }
                else{
                    qObj.question = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("questionDate") as! NSString)) {
                    qObj.questionDate = obj.valueForKey("questionDate") as? NSString;
                }
                else{
                    qObj.questionDate = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("expiryDate") as! NSString)) {
                    qObj.expiryDate = obj.valueForKey("expiryDate") as? NSString;
                }
                else{
                    qObj.expiryDate = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("textOptionOne") as! NSString)) {
                    qObj.textOptionOne = obj.valueForKey("textOptionOne") as? NSString;
                }
                else{
                    qObj.textOptionOne = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("textOptionTwo") as! NSString)) {
                    qObj.textOptionTwo = obj.valueForKey("textOptionTwo") as? NSString;
                }
                else{
                    qObj.textOptionTwo = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("textOptionThree") as! NSString)) {
                    qObj.textOptionThree = obj.valueForKey("textOptionThree") as? NSString;
                }
                else{
                    qObj.textOptionThree = nil;
                }
                if (!self.isStringEmpty(obj.valueForKey("textOptionFour") as! NSString)) {
                    qObj.textOptionFour = obj.valueForKey("textOptionFour") as? NSString;
                }
                else{
                    qObj.textOptionFour = nil;
                }
                qObj.isType = obj.valueForKey("isType") as? NSNumber
                qObj.expiryTime = obj.valueForKey("expiryTime") as? NSNumber
                qObj.optionCount = obj.valueForKey("optionCount") as? NSNumber
                qObj.imageOptionOne = nil
                qObj.imageOptionTwo = nil
                qObj.imageOptionThree = nil
                qObj.imageOptionFour = nil
                arr.append(qObj)
            }
            
        }
        
        
        return arr;
    }
    
    
    //MARK: Settings Methods
    
    func isSettingsAlreadyExistsInDatabaseWithId(objId:NSString)->Bool{
        var array:NSArray = []
        var isExists:Bool = false
        let request = NSFetchRequest(entityName: "Settings")
        let resultPredicate = NSPredicate(format: "parseObjectId ==  %@", objId)
        request.predicate = resultPredicate
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        if (array.count > 0) {
            isExists = true;
        }
        else{
            isExists = false;
        }
        return isExists;
    }
    
    func saveSettingsObjIntoDatabase(sObject:SettingsModel){
        let prd = NSEntityDescription.insertNewObjectForEntityForName("Settings", inManagedObjectContext: context)
        
        if (!(self.isStringEmpty(sObject.objectId!))){
            prd.setValue(sObject.objectId, forKey: "parseObjectId")
        }
        prd.setValue(sObject.redExpiryTime, forKey: "redExpiryTime")
        prd.setValue(sObject.questionInMyQuestionVisibility, forKey: "questionInMyQuestionsVisibility")
        prd.setValue(sObject.questionInInboxVisibility, forKey: "questionInInboxVisibility")
        prd.setValue(sObject.questionExpiryTimer, forKey: "questionExpiryTimer")
        
        // prd.setValue(sObject.questionExpiryTime, forKey: "questionExpiryTime")
        // prd.setValue(sObject.beforeRedTime, forKey: "beforeRedTime")
        // prd.setValue(sObject.afterRedTime, forKey: "afterRedTime")
        //  prd.setValue(sObject.questionInExploreVisibility, forKey: "questionInExploreVisibility")

        
        do {
            try self.context.save()
        } catch let error as NSError  {
            print("Could not save question object: \(error), \(error.userInfo)")
        }
    }
    
    
    func getSettingObjFromCoredata()->SettingsModel{
        let sObj:SettingsModel = SettingsModel()
        var array:NSArray = []
        
        let request = NSFetchRequest(entityName: "Settings")
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        if array.count > 0 {
            for sett in array {
                let obj = sett as! NSManagedObject
                
                if (!self.isStringEmpty(obj.valueForKey("parseObjectId") as! NSString)) {
                    sObj.objectId = obj.valueForKey("parseObjectId") as? NSString;
                }
                else{
                    sObj.objectId = nil;
                }
                sObj.redExpiryTime = obj.valueForKey("redExpiryTime") as? NSNumber
                sObj.questionInMyQuestionVisibility = obj.valueForKey("questionInMyQuestionsVisibility") as? NSNumber
                sObj.questionInInboxVisibility = obj.valueForKey("questionInInboxVisibility") as? NSNumber
                sObj.questionExpiryTimer = obj.valueForKey("questionExpiryTimer") as? NSNumber

            }
            
        }
        
        return sObj;

    }
    
    
    func updateSettingsObjIntoCoredata(setObj:SettingsModel){
        let request = NSFetchRequest(entityName: "Settings")
        let resultPredicate = NSPredicate(format: "parseObjectId ==  %@", setObj.objectId!)
        request.predicate = resultPredicate
        var array:NSArray = []
        do {
            let results =
            try self.managedObjectContextObj.executeFetchRequest(request)
            array = results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        if array.count == 1{
            let obj:NSManagedObject = array.objectAtIndex(0) as! NSManagedObject
            obj.setValue(setObj.redExpiryTime, forKey: "redExpiryTime")
            obj.setValue(setObj.questionInInboxVisibility, forKey: "questionInMyQuestionsVisibility")
            obj.setValue(setObj.questionInMyQuestionVisibility, forKey: "questionInInboxVisibility")
            obj.setValue(setObj.questionExpiryTimer, forKey: "questionExpiryTimer")
        }
        
        do {
            try self.context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        
    }
    
    
  /*  if (Reachability.isConnectedToNetwork()){
    
    }
    else{
    let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
    alertView.show()
    }*/

    
    
    
    //Mark check Empty String
    func isStringEmpty(string:NSString)->Bool{
        if ( string.isKindOfClass(NSNull) || string == "" || string == "<null>" || NSString(format: "%@",string).length == 0){
            return true
        }
        return false
    }

    
 }
