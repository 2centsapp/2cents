//
//  ContactGroups+CoreDataProperties.m
//  CentsApp
//
//  Created by Sagar Jadhav on 1/25/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ContactGroups+CoreDataProperties.h"

@implementation ContactGroups (CoreDataProperties)

@dynamic parseObjectId;
@dynamic groupName;
@dynamic owner;
@dynamic memberIds;

@end
