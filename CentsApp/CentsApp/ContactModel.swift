//
//  ContactModel.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 12/28/15.
//  Copyright © 2015 Rapidera. All rights reserved.
//

import UIKit

class ContactModel: NSObject {
    var objectId:NSString?
    var firstName:NSString?
    var lastName:NSString?
    var phoneNumber:NSString?
    var email:NSString?
}
