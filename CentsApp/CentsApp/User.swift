//
//  User.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 11/3/15.
//  Copyright (c) 2015 Sagar Jadhav. All rights reserved.
//

import UIKit
import Parse
class User: PFObject {
    var objectID:NSString!
    var firstName:NSString!
    var lastName:NSString!
    var password:NSString!
    var username:NSString!
    var email:NSString!
    var referralId:NSString!
    var phoneNumber:NSString!
    var isVerified:NSNumber!
    var accountType:NSNumber!
    var profilePicture:PFFile!
    
    
    override init() {
        super.init()
    }
    
//    class func createEmptyUser()-> User{
//        //let user:User!
//        //return user;
//    }
    /*
    class func convertPFObjectToUser(userObject:PFObject)-> User{
        var user:User!
        user.objectId = userObject.objectId
        //user.firstName = ([self .isPopulatedString(USER_FIRST_NAME)]??userObject.objectForKey(USER_FIRST_NAME)::"")
        user.lastName  = userObject.objectForKey(USER_LAST_NAME)?.string
        user.password = userObject.objectForKey(USER_PASSWORD)?.string
        user.username = userObject.objectForKey(USER_USERNAME)?.string
        user.email = userObject.objectForKey(USER_EMAIL)?.string
        user.referralId = userObject.objectForKey(USER_REFERRAL_ID)?.string
        user.phoneNumber = userObject.objectForKey(USER_PHONE_NUMBER)?.string
        user.isVerified = userObject.objectForKey(USER_IS_VERIFIED)?.integerValue
        user.accountType = userObject.objectForKey(USER_ACCOUNT_TYPE)?.integerValue
       // user.profilePicture = userObject.objectForKey(USER_PROFILE_PICTURE)
        return user;
    }*/
    
//    func isPopulatedString(str:NSString)->Bool{
//        var bl:Bool = NO;
//        if(str.length>0){
//            bl = YES;
//        }
//        return bl
//    }
    
//    func convertUserToPFUser()->PFUser{
//     var user:PFUser;
//        
//        return user;
//    }
}
