//
//  SettingsViewController.swift
//  CentsApp
//
//  Created by Mahesh on 11/3/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import Parse
class SettingsViewController: UIViewController,UITextFieldDelegate {
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet var txtFld1: UITextField!
    @IBOutlet var txtFld2: UITextField!
    @IBOutlet var txtFld3: UITextField!
    @IBOutlet var txtFld4: UITextField!
    @IBOutlet var btnSave: UIButton!

    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    let singleton = Singleton.sharedInstance
    let coredata = CoreDataManager.sharedInstance
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "SettingsViewRefresh", name: "SettingsRefresh", object: nil)

        let btn   = UIButton(type:UIButtonType.System) as UIButton
        btn.frame = CGRectMake(0, 0, self.view.frame.size.width,65)
        btn.setTitle("Settings", forState: .Normal)  //set button title
        btn.setTitleColor(UIColor.blackColor(),forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.addTarget(self, action: "buttonSettingTapped:", forControlEvents: .TouchUpInside) //add button action
        
        self.view.addSubview(btn)
        
        let maskPath = UIBezierPath(roundedRect:self.view.bounds, byRoundingCorners: [UIRectCorner.TopLeft , UIRectCorner.TopRight], cornerRadii: CGSizeMake(10.0, 10.0))
        
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.CGPath
        self.view.layer.mask = maskLayer
        self.view.addSubview(btn)
        if (self.appDelegateObj.settingObject != nil){
            if (self.appDelegateObj.settingObject.questionExpiryTimer != nil){
                self.txtFld1.text = self.appDelegateObj.settingObject.questionExpiryTimer?.stringValue
            }
            if (self.appDelegateObj.settingObject.redExpiryTime != nil){
                self.txtFld2.text = self.appDelegateObj.settingObject.redExpiryTime?.stringValue
            }
            if (self.appDelegateObj.settingObject.questionInInboxVisibility != nil){
                self.txtFld3.text = self.appDelegateObj.settingObject.questionInInboxVisibility?.stringValue
            }
            if (self.appDelegateObj.settingObject.questionInMyQuestionVisibility != nil){
                self.txtFld4.text = self.appDelegateObj.settingObject.questionInMyQuestionVisibility?.stringValue
            }
        }
        print(self.appDelegateObj.loggedInUser?.objectForKey("phoneNumber"))
        if (self.appDelegateObj.loggedInUser != nil){
            if (self.appDelegateObj.loggedInUser?.objectForKey("phoneNumber") != nil){
                self.txtPhoneNumber.text = (self.appDelegateObj.loggedInUser?.objectForKey("phoneNumber")) as? String
            }
            if (self.appDelegateObj.loggedInUser?.email != nil){
                self.txtEmail.text = self.appDelegateObj.loggedInUser!.email
            }
            if (self.appDelegateObj.loggedInUser?.objectForKey("firstName") != nil){
                self.txtFirstName.text = (self.appDelegateObj.loggedInUser?.objectForKey("firstName")) as? String
            }
            if (self.appDelegateObj.loggedInUser?.objectForKey("lastName") != nil){
                self.txtLastName.text = (self.appDelegateObj.loggedInUser?.objectForKey("lastName")) as? String
            }
        }
        
        //let se:SettingsModel = self.coredata.getSettingObjFromCoredata()
       // print(se.objectId,se.redExpiryTime,se.questionExpiryTimer,se.questionInInboxVisibility,se.questionInMyQuestionVisibility)
    }
    
    func customViewWillAppear() {
        
    }
    func buttonSettingTapped(sender:UIButton!)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("SettingTapped", object: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnSaveClicked(sender: UIButton){
         self.hideKeyboard()
        if(self.isValidateSettings()){
            if (Reachability.isConnectedToNetwork()){
                if (self.appDelegateObj.settingObject != nil ){ // update
                    self.appDelegateObj.showIndicatorView()
                    
                    let query = PFQuery(className:"Settings")
                    query.getObjectInBackgroundWithId(self.appDelegateObj.settingObject.objectId! as String) {
                        (object, error) -> Void in
                        if error != nil {
                        } else {
                            if let object = object {
                                object["questionExpiryTimer"] = Int(self.txtFld1.text!)
                                object["redExpiryTime"] = Int(self.txtFld2.text!)
                                object["questionInInboxVisibility"] =  Int(self.txtFld3.text!)
                                object["questionInMyQuestionsVisibility"] =  Int(self.txtFld4.text!)
                            }
                            
                            object!.saveInBackground()
                            self.appDelegateObj.settingObject.redExpiryTime = Int(self.txtFld2.text!)
                            self.appDelegateObj.settingObject.questionExpiryTimer = Int(self.txtFld1.text!)
                            self.appDelegateObj.settingObject.questionInInboxVisibility = Int(self.txtFld3.text!)
                            self.appDelegateObj.settingObject.questionInMyQuestionVisibility = Int(self.txtFld4.text!)
                            
                            let alert = UIAlertController(title: "Success", message: "Settings updated successfully", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
                            self.presentViewController(alert, animated: true, completion: {
                                
                            })
                            //update settings obejct into coredata
                            if(self.coredata.isSettingsAlreadyExistsInDatabaseWithId(object!.objectId!)){
                                self.coredata.updateSettingsObjIntoCoredata(self.appDelegateObj.settingObject);
                            }
                            
                            
                            
                        }
                        self.appDelegateObj.hideIndicatorView()
                        
                    }
                }
                else{ //enter new entry
                    self.appDelegateObj.showIndicatorView()
                    
                    var post:PFObject!
                    post  = PFObject(className: "Settings")
                    post["userId"] = self.appDelegateObj.loggedInUser?.objectId
                    
                    if (txtFld1.text?.characters.count > 0){
                        post["questionExpiryTimer"] = Int(txtFld1.text!)
                    }
                    if (txtFld2.text?.characters.count > 0){
                        post["redExpiryTime"] = Int(txtFld2.text!)
                    }
                    if (txtFld3.text?.characters.count > 0){
                        post["questionInInboxVisibility"] =  Int(txtFld3.text!)
                    }
                    if (txtFld4.text?.characters.count > 0){
                        post["questionInMyQuestionsVisibility"] =  Int(txtFld4.text!)
                    }
                    
                    
                    post.saveInBackgroundWithBlock{
                        (success:Bool, error:NSError?)-> Void in
                        
                        if success == true{
                            print("uploaded successfully in settings")
                            let alert = UIAlertController(title: "Success", message: "Settings updated successfully", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
                            self.presentViewController(alert, animated: true, completion: {
                                
                            })
                            self.fetchSettingsFromParse()
                        }
                        else{
                            print("settings updation failed")
                            let alert = UIAlertController(title: "Failed", message:error?.description, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
                            self.presentViewController(alert, animated: true, completion: {
                                
                            })
                        }
                        self.appDelegateObj.hideIndicatorView()
                        
                    }
                }

            }
            else{
                let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
                alertView.show()
            }
        
        }
       
    }

    func fetchSettingsFromParse(){
        let compQuery = PFQuery(className:"Settings")
        compQuery.whereKey("userId", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if results?.count != 0{
                    if let settObjects:NSArray = results {
                        for sett in settObjects {
                            // Use staff as!a standard PFObject now. e.g.
                            let que:PFObject = sett as! PFObject
                            
                            self.appDelegateObj.settingObject = SettingsModel()
                            self.appDelegateObj.settingObject.objectId = que.objectId
                            self.appDelegateObj.settingObject.userId = que.objectForKey("userId") as!String
                            self.appDelegateObj.settingObject.redExpiryTime = que.objectForKey("redExpiryTime") as!Int
                            self.appDelegateObj.settingObject.questionExpiryTimer = que.objectForKey("questionExpiryTimer") as!Int
                            self.appDelegateObj.settingObject.questionInInboxVisibility = que.objectForKey("questionInInboxVisibility") as!Int
                            self.appDelegateObj.settingObject.questionInMyQuestionVisibility = que.objectForKey("questionInMyQuestionsVisibility") as!Int
                            
                            //SAVE SETTINGS INTO coredata
                            if(!self.coredata.isSettingsAlreadyExistsInDatabaseWithId(que.objectId!)){
                                self.coredata.saveSettingsObjIntoDatabase(self.appDelegateObj.settingObject);
                            }
                        }
                        
                    }
                }
            }
            else{
                print("Error in retrieving \(error)")
            }
            
            
            
        })
    }
    
    func SettingsViewRefresh(){
        print("Refresh setting view")
      //   self.hideKeyboard()
        let compQuery = PFQuery(className:"Settings")
        compQuery.whereKey("userId", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if results?.count != 0{
                    if let settObjects:NSArray = results {
                        for sett in settObjects {
                            // Use staff as!a standard PFObject now. e.g.
                            let que:PFObject = sett as! PFObject
                            
                            self.appDelegateObj.settingObject = SettingsModel()
                            self.appDelegateObj.settingObject.objectId = que.objectId
                            self.appDelegateObj.settingObject.userId = que.objectForKey("userId") as!String
                            self.appDelegateObj.settingObject.redExpiryTime = que.objectForKey("redExpiryTime") as!Int
                            self.appDelegateObj.settingObject.questionExpiryTimer = que.objectForKey("questionExpiryTimer") as!Int
                            self.appDelegateObj.settingObject.questionInInboxVisibility = que.objectForKey("questionInInboxVisibility") as!Int
                            self.appDelegateObj.settingObject.questionInMyQuestionVisibility = que.objectForKey("questionInMyQuestionsVisibility") as!Int
                            
                            
                            if (self.appDelegateObj.settingObject != nil){
                                if (self.appDelegateObj.settingObject.questionExpiryTimer != nil){
                                    self.txtFld1.text = self.appDelegateObj.settingObject.questionExpiryTimer?.stringValue
                                }
                                if (self.appDelegateObj.settingObject.redExpiryTime != nil){
                                    self.txtFld2.text = self.appDelegateObj.settingObject.redExpiryTime?.stringValue
                                }
                                if (self.appDelegateObj.settingObject.questionInInboxVisibility != nil){
                                    self.txtFld3.text = self.appDelegateObj.settingObject.questionInInboxVisibility?.stringValue
                                }
                                if (self.appDelegateObj.settingObject.questionInMyQuestionVisibility != nil){
                                    self.txtFld4.text = self.appDelegateObj.settingObject.questionInMyQuestionVisibility?.stringValue
                                }
                            }
                        }
                        
                    }
                }
            }
            else{
                print("Error in retrieving \(error)")
            }
            
            
            
            
        })
        
        
        
    }
    // MARK: Button Action Methods
    @IBAction func btnSaveProfile(sender: UIButton) {
        self.hideKeyboard()
        if self.isValidateProfile(){
            if (Reachability.isConnectedToNetwork()){
                self.appDelegateObj.showIndicatorView()
                let userInfo = PFUser.currentUser()
                userInfo?.setValue(self.txtPhoneNumber.text, forKey: "phoneNumber")
                userInfo?.setValue(self.txtFirstName.text, forKey: "firstName")
                userInfo?.setValue(self.txtLastName.text, forKey: "lastName")
                userInfo?.email  = self.txtEmail.text
                userInfo?.username  = self.txtPhoneNumber.text
                userInfo!.saveInBackgroundWithBlock {
                    (success:Bool, error:NSError?)-> Void in
                    if success == true{
                        print("profile updated successfully ")
                        let alert = UIAlertController(title: "Success", message: "Profile updated successfully", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
                        self.presentViewController(alert, animated: true, completion: {
                            
                        })
                        self.appDelegateObj.hideIndicatorView()
                        self.appDelegateObj.loggedInUser = userInfo
                        let userDefaults = NSUserDefaults.standardUserDefaults()
                        userDefaults.setValue(self.txtPhoneNumber.text, forKey: "UserName")
                        userDefaults.setValue(self.txtPhoneNumber.text, forKey: "PhoneNumber")
                        userDefaults.synchronize()
                    }
                    else{
                        print("profile updation failed")
                        let alert = UIAlertController(title: "Failed", message:error?.description, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
                        self.presentViewController(alert, animated: true, completion: {
                            
                        })
                    }
                    
                }
            }
            else{
                let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
                alertView.show()
            }
            

        }
       
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    func hideKeyboard(){

        self.txtFld1.resignFirstResponder()
        self.txtFld2.resignFirstResponder()
        self.txtFld3.resignFirstResponder()
        self.txtFld4.resignFirstResponder()
        self.txtPhoneNumber.resignFirstResponder()
        self.txtEmail.resignFirstResponder()
        self.txtFirstName.resignFirstResponder()
        self.txtLastName.resignFirstResponder()

        
    }
    
    func isValidateSettings()->Bool{
        
        if ((txtFld1.text?.characters.count == 0) || (txtFld2.text?.characters.count == 0)||(txtFld3.text?.characters.count == 0) || (txtFld4.text?.characters.count == 0)){
            let alert = UIAlertController(title: "Alert", message:"Please enter all settings values before saving.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                
            })
            return false
        }
        else if ( (!(singleton.isNumberValid(txtFld1.text!))) || (!(singleton.isNumberValid(txtFld2.text!))) || (!(singleton.isNumberValid(txtFld3.text!))) || (!(singleton.isNumberValid(txtFld4.text!)))){
            let alert = UIAlertController(title: "Alert", message:"Please enter numbers, alphabates are not allowed.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                
            })
            return false
        }
        return true
    }
    func isValidateProfile()->Bool{
        if ((txtPhoneNumber.text?.characters.count == 0) || (txtEmail.text?.characters.count == 0)||(txtFirstName.text?.characters.count == 0) || (txtLastName.text?.characters.count == 0)){
            let alert = UIAlertController(title: "Alert", message:"Fill up all profile data before saving", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                
            })
            return false
        }
        else if ((!(singleton.isPhoneNumberValid(txtPhoneNumber.text!))) || (txtPhoneNumber.text?.characters.first != "+")){
            let alert = UIAlertController(title: "Alert", message:"Please enter valid phone number", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                
            })
            return false
        }
        else if ((!(singleton.validateEmailWithString(txtEmail.text!)))){
            let alert = UIAlertController(title: "Alert", message:"Please enter valid email", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                
            })
            return false
        }
        else if ((!(singleton.isAphaNumericValid(txtFirstName.text!))) || (!(singleton.isAphaNumericValid(txtLastName.text!)))){
            let alert = UIAlertController(title: "Alert", message:"Please enter valid first name or last name", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
                
            })
            return false
        }
        return true
    }
    
}
