//
//  Questions+CoreDataProperties.h
//  CentsApp
//
//  Created by Sagar Jadhav on 1/21/16.
//  Copyright © 2016 Rapidera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Questions.h"

NS_ASSUME_NONNULL_BEGIN

@interface Questions (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *parseObjectId;
@property (nullable, nonatomic, retain) NSString *question;
@property (nullable, nonatomic, retain) NSNumber *isType;
@property (nullable, nonatomic, retain) NSNumber *expiryTime;
@property (nullable, nonatomic, retain) NSString *questionDate;
@property (nullable, nonatomic, retain) NSString *owner;
@property (nullable, nonatomic, retain) NSNumber *optionCount;
@property (nullable, nonatomic, retain) NSString *expiryDate;
@property (nullable, nonatomic, retain) NSString *textOptionOne;
@property (nullable, nonatomic, retain) NSString *textOptionTwo;
@property (nullable, nonatomic, retain) NSString *textOptionThree;
@property (nullable, nonatomic, retain) NSString *textOptionFour;
@property (nullable, nonatomic, retain) NSData *imageOptionOne;
@property (nullable, nonatomic, retain) NSData *imageOptionTwo;
@property (nullable, nonatomic, retain) NSData *imageOptionThree;
@property (nullable, nonatomic, retain) NSData *imageOptionFour;
@property (nullable, nonatomic, retain) NSNumber *isExpired;

@end

NS_ASSUME_NONNULL_END
