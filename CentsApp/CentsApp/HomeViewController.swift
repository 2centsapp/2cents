//
//  HomeViewController.swift
//  CentsApp
//
//  Created by Mahesh on 11/3/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController
{
    var j : Int!
    @IBOutlet weak var containerInbox: UIView!
    @IBOutlet weak var containerMyQuestions: UIView!
    @IBOutlet weak var containerExplorer: UIView!
    @IBOutlet weak var containerFriends: UIView!
    @IBOutlet weak var containerSettings: UIView!
    @IBOutlet weak var containerAskQuestions: UIView!
    
    @IBOutlet var inboxBottomConstraint: NSLayoutConstraint!
    @IBOutlet var myQuesnBottomConstraint: NSLayoutConstraint!
    @IBOutlet var explorerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var friendsBottomConstraint: NSLayoutConstraint!
    @IBOutlet var settingsBottomConstraint: NSLayoutConstraint!
    @IBOutlet var askQuesnBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var inboxheightConstraint: NSLayoutConstraint!
    @IBOutlet var myquestionheightConstraint: NSLayoutConstraint!
    
    @IBOutlet var exploreheightConstraint: NSLayoutConstraint!
    @IBOutlet var friendsheightConstraint: NSLayoutConstraint!
    
    @IBOutlet var settingsheightConstraint: NSLayoutConstraint!
    
    @IBOutlet var askquestionheightConstraint: NSLayoutConstraint!
    
    //MARK: Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "InboxTappedSelector", name: "InboxTapped", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "MYQTappedSelector", name: "MYQTapped", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "ExploreTappedSelector", name: "ExploreTapped", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "FriendsTappedSelector", name: "FriendsTapped", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "SettingTappedSelector", name: "SettingTapped", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "AskQuestionsTappedSelector", name: "AskQuestionsTapped", object: nil)
        
        j=0;
        
        
        
    }
    override func viewDidDisappear(animated: Bool)
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "InboxTapped", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "MYQTapped", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ExploreTapped", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "FriendsTapped", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "SettingTapped", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "AskQuestionsTapped", object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "settingsSegue") {
            print("-------segue called")
        }
    }

    //MARK: View Methods
    
    func buttonClicked(){

        UIView.animateWithDuration(0.5, animations: {
            self.containerAskQuestions.frame = CGRectMake(self.view.frame.origin.x + 2, self.view.frame.size.height - 60, self.view.frame.size.width - 4, 60)
            self.askquestionheightConstraint.constant = 60
            self.askQuesnBottomConstraint.constant = 00
        })
        UIView.animateWithDuration(0.5, animations: {
            self.containerSettings.frame = CGRectMake(self.view.frame.origin.x+2,(self.containerAskQuestions.frame.origin.y-60)+8,self.view.frame.size.width-4, 60)
            self.settingsheightConstraint.constant = 60
            self.settingsBottomConstraint.constant = 47
        })
        UIView.animateWithDuration(0.5, animations: {
            self.containerFriends.frame = CGRectMake(self.view.frame.origin.x+2,(self.containerSettings.frame.origin.y-60)+8,self.view.frame.size.width-4, 60)
            self.friendsheightConstraint.constant = 60
            self.friendsBottomConstraint.constant = 99
        })
        UIView.animateWithDuration(0.5, animations: {
            self.containerExplorer.frame = CGRectMake(self.view.frame.origin.x+2,(self.containerFriends.frame.origin.y-60)+8,self.view.frame.size.width-4, 60)
            self.explorerBottomConstraint.constant = 151
            self.exploreheightConstraint.constant = 60
        })
        UIView.animateWithDuration(0.5, animations: {
            self.containerMyQuestions.frame = CGRectMake(self.view.frame.origin.x+2, (self.containerExplorer.frame.origin.y-60)+8,self.view.frame.size.width-4, 60);
            self.myQuesnBottomConstraint.constant = 203
            self.myquestionheightConstraint.constant = 60

        })
        UIView.animateWithDuration(0.5, animations: {
            self.containerInbox.frame = CGRectMake(self.view.frame.origin.x+2,(self.containerMyQuestions.frame.origin.y-60)+8,self.view.frame.size.width-4, 60)
            self.inboxBottomConstraint.constant = 255
            self.inboxheightConstraint.constant = 60
        })
        self.view.backgroundColor = UIColor.blackColor()
    }
    
    // MARK: - Inbox Button Click Action
    func InboxTappedSelector(){
        
        if j==0{
            UIView.animateWithDuration(0.5, delay: 0.4, options: .TransitionNone, animations: {
                self.inboxheightConstraint.constant = self.view.frame.size.height-40
                self.inboxBottomConstraint.constant = 40
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.view.backgroundColor = UIColor.orangeColor()
                    NSNotificationCenter.defaultCenter().postNotificationName("InboxRefresh", object: nil)

            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerAskQuestions.frame = CGRectMake(0, self.view.frame.size.height-10, self.view.frame.size.width,13);
                self.askquestionheightConstraint.constant = 8
                self.askQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerSettings.frame = CGRectMake(0, self.containerAskQuestions.frame.origin.y-10,self.view.frame.size.width, 13);
                self.settingsheightConstraint.constant = 16
                self.settingsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerFriends.frame = CGRectMake(0, self.containerSettings.frame.origin.y-10,self.view.frame.size.width, 13);
                self.friendsheightConstraint.constant = 24
                self.friendsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerExplorer.frame = CGRectMake(0, self.containerFriends.frame.origin.y-10,self.view.frame.size.width, 13);
                self.exploreheightConstraint.constant = 32
                self.explorerBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerMyQuestions.frame = CGRectMake(0, self.containerExplorer.frame.origin.y-10, self.view.frame.size.width, 13);
                self.myquestionheightConstraint.constant = 40
                self.myQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            j=1
        }
        else if j==1{
            j = 0
            self.buttonClicked()
        }
    }
    
    // MARK: - My Question Button Click Action
    func MYQTappedSelector(){
        if (j==0){
            UIView.animateWithDuration(0.5, delay: 0.4, options: .TransitionNone, animations: {
                self.myquestionheightConstraint.constant = self.view.frame.size.height-40
                self.myQuesnBottomConstraint.constant = 40
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.view.backgroundColor = UIColor.greenColor()
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("MyQuestionsRefresh", object: nil)
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.askquestionheightConstraint.constant = 08
                self.askQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.settingsheightConstraint.constant = 16
                self.settingsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerFriends.frame = CGRectMake(0, self.containerSettings.frame.origin.y-10,self.view.frame.size.width, 13);
                self.friendsheightConstraint.constant = 24
                self.friendsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.exploreheightConstraint.constant = 32
                self.explorerBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.inboxheightConstraint.constant = 40
                self.inboxBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            j=1;
        }
        else if (j==1){
            j=0;
            self.buttonClicked()
        }
    }
    
    // MARK: - Explorer Button Click Action
    func ExploreTappedSelector(){
        if (j==0){
            UIView.animateWithDuration(0.5, delay: 0.4, options: .TransitionNone, animations: {
                self.containerExplorer.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width,self.view.frame.size.height-40);
                self.exploreheightConstraint.constant = self.view.frame.size.height-40
                self.explorerBottomConstraint.constant = 40
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.view.backgroundColor = UIColor.whiteColor()
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.askquestionheightConstraint.constant = 08
                self.askQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.settingsheightConstraint.constant = 16
                self.settingsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.friendsheightConstraint.constant = 24
                self.friendsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.myquestionheightConstraint.constant = 32
                self.myQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.inboxheightConstraint.constant = 40
                self.inboxBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            j=1;
        }
        else if (j==1){
            j=0;
            self.buttonClicked()
        }
    }
    
    // MARK: - Friends Button Click Action
    func FriendsTappedSelector(){
        if (j==0){
            UIView.animateWithDuration(0.5, delay: 0.4, options: .TransitionNone, animations: {
                self.friendsheightConstraint.constant = self.view.frame.size.height-65
                self.friendsBottomConstraint.constant = 65
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.view.backgroundColor = UIColor.cyanColor()
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("FriendsRefresh", object: nil)

            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.askquestionheightConstraint.constant = 08
                self.askQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.settingsheightConstraint.constant = 16
                self.settingsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.exploreheightConstraint.constant = 24
                self.explorerBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.myquestionheightConstraint.constant = 32
                self.myQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.inboxheightConstraint.constant = 40
                self.inboxBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            j=1
        }
        else if (j==1){
            j=0;
            self.buttonClicked()
        }
    }
    
    // MARK: - Settings Button Click Action
    func SettingTappedSelector(){
        if (j==0){
            UIView.animateWithDuration(0.5, delay: 0.4, options: .TransitionNone, animations: {
                self.settingsheightConstraint.constant = self.view.frame.size.height-65
                self.settingsBottomConstraint.constant = 65
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.view.backgroundColor = UIColor(red: 228/255, green: 170/255, blue: 227/255, alpha: 1.0)
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.askquestionheightConstraint.constant = 08
                self.askQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.containerFriends.frame = CGRectMake(0, self.containerSettings.frame.origin.y-10,self.view.frame.size.width, 13);
                self.friendsheightConstraint.constant = 16
                self.friendsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
                    NSNotificationCenter.defaultCenter().postNotificationName("SettingsRefresh", object: nil)
                    
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.exploreheightConstraint.constant = 24
                self.explorerBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.myquestionheightConstraint.constant = 32
                self.myQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.inboxheightConstraint.constant = 40
                self.inboxBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            j=1;
        }
        else if (j==1){
            j=0;
            self.buttonClicked()
        }
    }
    
    // MARK: - ASK Questions Button Click Action
    func AskQuestionsTappedSelector(){
        if (j==0){
            UIView.animateWithDuration(0.5, delay: 0.4, options: .TransitionNone, animations: {
                self.askquestionheightConstraint.constant = self.view.frame.size.height-65
                self.askQuesnBottomConstraint.constant = 65
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.view.backgroundColor = UIColor(red: 188/255, green: 255/255, blue: 32/255, alpha: 1.0)
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.settingsheightConstraint.constant = 08
                self.settingsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.friendsheightConstraint.constant = 16
                self.friendsBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.exploreheightConstraint.constant = 24
                self.explorerBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.myquestionheightConstraint.constant = 32
                self.myQuesnBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            
            UIView.animateWithDuration(0.4, delay: 0.4, options: .TransitionNone, animations: {
                self.inboxheightConstraint.constant = 40
                self.inboxBottomConstraint.constant = 00
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    //print("Basket doors opened!")
            })
            j=1;
        }
        else if(j==1){
            j=0;
            self.buttonClicked()
        }
    }
    
    
    
    
}
