//
//  FriendsViewController.swift
//  CentsApp
//
//  Created by Mahesh on 11/3/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import AddressBook
import MobileCoreServices
import Parse
class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    var addressBook: ABAddressBookRef?
    var arrParseContacts:NSMutableArray = [];
    var selectedIndexArray:NSMutableArray = [];
    var tField: UITextField!
    var isGroupCreating: Bool = false
    var filteredArray:[ContactModel]! = []
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet var tblView: UITableView!
    var searchActive : Bool = false
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate

     //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "FriendsViewRefresh", name: "FriendsRefresh", object: nil)

        let btn   = UIButton(type:UIButtonType.System)
        btn.frame = CGRectMake(0, 0, self.view.frame.size.width,65)
        btn.setTitle("Friends", forState: .Normal)  //set button title
        btn.setTitleColor(UIColor.blackColor(),forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.addTarget(self, action: "buttonFriendsTapped:", forControlEvents: .TouchUpInside) //add button action
        self.view.addSubview(btn)
        let maskPath = UIBezierPath(roundedRect:self.view.bounds, byRoundingCorners: [UIRectCorner.TopLeft , UIRectCorner.TopRight], cornerRadii: CGSizeMake(10.0, 10.0))
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.CGPath
        self.view.layer.mask = maskLayer
        self.view.addSubview(btn)
        tblView.delegate = self
        searchbar.delegate = self
        ContactsImporter.importContacts(showContacts)
        
        //sample code to store data into coredata - logged in user data is saved into User table of CoreData
        //let coredata = CoreDataManager.sharedInstance
        //coredata.saveSelfUserData(self.appDelegateObj.loggedInUser!)
       // print(coredata.getAllUSerData())
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
        } else {
            print("Internet connection FAILED")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func FriendsViewRefresh(){
        print("Refresh friends view")
        tblView.reloadData()
    
    }
    
    //MARK: Contacts Methods

    func createContactModel(contacts: NSArray){
        for var i = 0; i < contacts.count; i++
        {
            let contactm = ContactModel()
            let obj:PFObject = contacts.objectAtIndex(i) as! PFObject
        
            contactm.firstName = obj.valueForKey("firstName") as! String
            contactm.lastName = obj.valueForKey("lastName") as! String
            contactm.phoneNumber = obj.valueForKey("phoneNumber") as! String
           // contactm.email = obj.valueForKey("email") as! String
            contactm.objectId = obj.objectId
            arrParseContacts.addObject(contactm)
        }
        tblView.reloadData()
    }
    
    func showContacts(contacts: Array<Contact>, error: NSError!) {
        print(contacts)
        if contacts.count > 50{
            let contactSmallArray:NSArray = contacts
            let arr:NSArray =  contactSmallArray.subarrayWithRange(NSRange(location: 0,length: 49))
            self.getContactsFromDatabase(arr as! Array<Contact>)
        }
        else{
            self.getContactsFromDatabase(contacts)
        }
        
        
        dispatch_async(dispatch_get_main_queue(), {
            if(error == nil) {
                // let alertView = UIAlertView(title: "Success!", message: "\(contacts.count) contacts imported successfully", delegate: nil, cancelButtonTitle: "OK")
                // alertView.show()
            }
            else {
                let alertView = UIAlertView(title: "Error \(error.code)",
                    message: "[\(error.code)] \(error.localizedDescription)\n\n\(contacts.count) Unable to import contacts",
                    delegate: nil,
                    cancelButtonTitle: "OK")
                alertView.show()
            }
        })
    }
    
    //MARK: Parse Methods
    func getContactsFromDatabase(contacts: Array<Contact>)
    {
        //self.appDelegateObj.showIndicatorView()

        let compQuery = PFQuery(className:"Contacts")
        compQuery.whereKey("userId", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if let arr:NSArray = results {
                    if arr.count == contacts.count{
                        print("Contacts are already uptodate")
                        self.createContactModel(arr)
                        
                        
                    }else{
                        print("Contacts are need to update")
                        if let arr:NSArray = results {
                        for var i = 0; i < arr.count; i++
                        {
                            var object:PFObject!
                            object = arr.objectAtIndex(i) as! PFObject
                            object.deleteInBackground()

                        }
                        
                        
                        }
                        self.saveContactsToDatabase(contacts);
                    }
                }
            }
            else{
                print("Error in retrieving \(error)")
            }
            self.appDelegateObj.hideIndicatorView()

            
        })
    }
    
    func saveContactsToDatabase(contacts: NSArray)
    {

        for var i = 0; i < contacts.count; i++
        {
            var post:PFObject!
            let cn:Contact = contacts.objectAtIndex(i) as! Contact
            post  = PFObject(className: "Contacts")
            post["userId"] = self.appDelegateObj.loggedInUser?.objectId
            post["firstName"] = "\(cn.name)"
            post["lastName"] = "\(cn.lastName!)"
//            let arr:NSArray = cn.emailsArray!
//            if arr.count > 0 {
//                let dict:NSDictionary = arr.objectAtIndex(0) as! NSDictionary
//                let a:NSArray =  dict.allValues as NSArray
//                
//                post["email"] = a.objectAtIndex(0)
//            }
//            else{
//                post["email"] = "NA"
//            }
            print("\(cn.phonesArray)")

            let phone:NSArray = cn.phonesArray!
            if phone.count > 0 {
                let dict:NSDictionary = phone.objectAtIndex(0) as! NSDictionary
                let a:NSArray =  dict.allValues as NSArray
                
                post["phoneNumber"] = a.objectAtIndex(0)
            }
            else{
                post["phoneNumber"] = "NA"
            }
           // post["lastName"] =
           // post["email"] =
           // post["phoneNumber"] =
            
            
            post.saveInBackground()
        }
        
    }
    
    //MARK: UIBUtton Action Methods
    func buttonFriendsTapped(sender:UIButton!)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("FriendsTapped", object: nil)
//        if let err = SD.createTable("Cities", withColumnNamesAndTypes: ["Name": .StringVal, "Population": .IntVal, "IsWarm": .BoolVal, "FoundedIn": .DateVal]) {
//            //there was an error during this function, handle it here
//        } else {
//            //no error, the table was created successfully
//        }
    }

    @IBAction func btnGroupsClicked(sender: AnyObject) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let groupsVC = storyBoard.instantiateViewControllerWithIdentifier("GroupsViewController") as!GroupsViewController
        self.navigationController?.pushViewController(groupsVC, animated: false)
    }
    
    @IBAction func btnCreateGroupClicked(sender: AnyObject) {
        
        self.searchActive = false;
        self.tblView.reloadData()
        self.searchbar.text = ""
        self.searchbar.resignFirstResponder()
        
        
        let alert = UIAlertController(title: "Ground Name", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            print("\n\nEntered Group Name : \(self.tField.text)")
            self.isGroupCreating = true
            self.tblView.reloadData()
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: UITableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchActive) {
            if self.isGroupCreating == true{
                return self.filteredArray.count + 1
            }
            else{
                return self.filteredArray.count
            }
        }
        else
        {
            if self.isGroupCreating == true{
                return self.arrParseContacts.count + 1
            }
            else{
                return self.arrParseContacts.count
            }
        }
        
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        if self.isGroupCreating == true{
            let cell:UITableViewCell = self.tblView.dequeueReusableCellWithIdentifier("cellFriends")! as UITableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.accessoryType = UITableViewCellAccessoryType.None
            
            
            if indexPath.row == self.arrParseContacts.count{
                //cell.textLabel?.text = "Create Group"
                let myFirstButton = UIButton()
                myFirstButton.setTitle("Create", forState: .Normal)
                myFirstButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                myFirstButton.backgroundColor = UIColor.blackColor()
                myFirstButton.frame = CGRectMake(00, 00, cell.contentView.frame.size.width, cell.contentView.frame.size.height)
                myFirstButton.addTarget(self, action: "createGroundCellButtonPressed:", forControlEvents: .TouchUpInside)
                cell.contentView.addSubview(myFirstButton)
            }
            else{
                let contactModel:ContactModel! = self.arrParseContacts[indexPath.row] as! ContactModel
                cell.textLabel?.font = UIFont.systemFontOfSize(13.0)
                cell.textLabel?.text = "\(contactModel.firstName!) \(contactModel.lastName!)"
            }
            return cell
        }
        else{
            let cell:UITableViewCell = self.tblView.dequeueReusableCellWithIdentifier("cellFriends")! as UITableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.accessoryType = UITableViewCellAccessoryType.None

            
            let contactModel:ContactModel! = self.arrParseContacts[indexPath.row] as! ContactModel
            cell.textLabel?.font = UIFont.systemFontOfSize(11.0)
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.text = "\(contactModel.firstName!) \(contactModel.lastName!) \nPhone: \(contactModel.phoneNumber!)"
            return cell
        }
    }
    
    func createGroundCellButtonPressed(sender: UIButton!) {
        self.isGroupCreating = false
        self.tblView.reloadData()
        
        let alertView = UIAlertView();
        alertView.title = "Group";
        alertView.addButtonWithTitle("Ok");
        alertView.message = "Group Created Successfully";
        alertView.show();
        
        print(self.selectedIndexArray)
        
        //upload group on parse
        let temparray:NSMutableArray = [];
        
        for var i = 0; i < self.selectedIndexArray.count; i++
        {
            let cn:ContactModel = self.selectedIndexArray.objectAtIndex(i) as! ContactModel
            temparray.addObject(cn.objectId!)
            
        }
         
        var post:PFObject!
        post  = PFObject(className: "ContactGroups")
        post["owner"] = self.appDelegateObj.loggedInUser?.objectId
        post["groupName"] = self.tField.text
        post["memberIds"] = temparray
       
        post.saveInBackground()
        
        
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if self.isGroupCreating == true{
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            
            if indexPath.row != self.arrParseContacts.count{
                if (selectedIndexArray .containsObject(self.arrParseContacts[indexPath.row] as! ContactModel))
                {
                    selectedIndexArray.removeObject(self.arrParseContacts[indexPath.row] as! ContactModel)
                    cell!.accessoryType = UITableViewCellAccessoryType.None
                }
                else{
                    selectedIndexArray.addObject(self.arrParseContacts[indexPath.row] as! ContactModel)
                    cell!.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
            }
        }
    }
    
    //MARK: UISearchBar Methods
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText == ""
        {
            searchActive = false
        }
        else
        {
            searchActive = true
            let searchPredicate = NSPredicate(format: "firstName CONTAINS[c] %@", searchText)
            let array:[ContactModel]! = (self.arrParseContacts as NSArray).filteredArrayUsingPredicate(searchPredicate) as![ContactModel]
            filteredArray = array
            
        }
        self.tblView.reloadData()
    }
    
    
    //MARK: Other Methods
    func configurationTextField(textField: UITextField!){
        textField.placeholder = "Enter group name you wish to set"
        tField = textField
    }
    
    func handleCancel(alertView: UIAlertAction!){
        print("Cancelled !!")
        self.isGroupCreating = false
    }
}
