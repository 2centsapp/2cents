 //
//  ViewController.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 11/3/15.
//  Copyright (c) 2015 Sagar Jadhav. All rights reserved.
//

import UIKit
import Foundation
import Parse


class InboxTableVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet weak var searchbarInbox: UISearchBar!
    @IBOutlet var tableViewInbx: UITableView!
    @IBOutlet var btnAskQuestion: UIButton!
    var GlobalquestionObject:QuestionModel!
    var GLobalanswerObject:AnswerModel!
    var searchActive : Bool = false
    var filteredArray:[QuestionModel]! = []
    var items: [QuestionModel]! = []
    var answerArray: [AnswerModel]! = []
    
    let btn   = UIButton(type:UIButtonType.System)
    let coredata = CoreDataManager.sharedInstance
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "InboxViewRefresh", name: "InboxRefresh", object: nil)

        tableViewInbx.delegate = self
        searchbarInbox.delegate = self
        
        
        
        
        
        
        if (Reachability.isConnectedToNetwork()){
            self.parseCallToGetInboxData()
        }
        else{
            self.items = coredata.getQuestionsDataFromDatabase(true, owner: "")
            self.tableViewInbx.reloadData()
        }
       
        
        
        btn.frame = CGRectMake(0, 0, self.view.frame.size.width,65)
        btn.setTitle("Inbox", forState: .Normal)  //set button title
        btn.setTitleColor(UIColor.whiteColor(),forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.addTarget(self, action: "ButtonInboxTapped:", forControlEvents: .TouchUpInside) //add button action
        
        self.view.addSubview(btn)
        
        let maskPath = UIBezierPath(roundedRect:self.view.bounds, byRoundingCorners: [UIRectCorner.TopLeft , UIRectCorner.TopRight], cornerRadii: CGSizeMake(10.0, 10.0))
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.CGPath
        self.view.layer.mask = maskLayer
        self.view.addSubview(btn)

        
    }
    
    //MARK: Parse Methods
    
    func parseCallToGetInboxData(){
        self.appDelegateObj.showIndicatorView()

        let compQuery = PFQuery(className:"Questions")
        compQuery.whereKey("owner", notEqualTo:(self.appDelegateObj.loggedInUser?.objectId)!)
        
        compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
            if(error == nil){
                if let questionsObjects:NSArray = results {
                    for question in questionsObjects {
                        // Use staff as!a standard PFObject now. e.g.
                        let que:PFObject = question as! PFObject
                        
                        let questionObject = QuestionModel()
                        questionObject.objectId = que.objectId
                        questionObject.question = que.objectForKey("question") as!String
                        questionObject.isType = que.objectForKey("isType") as!Int
                        questionObject.textAnswer = que.objectForKey("textAnswer") as!String
                        questionObject.expiryTime = que.objectForKey("expiryTime") as!Int
                        questionObject.questionDate = que.objectForKey("questionDate") as!String
                        questionObject.expiryDate = que.objectForKey("expiryDate") as!String
                        
                        questionObject.owner = que.objectForKey("owner") as!String
                        questionObject.optionCount = que.objectForKey("optionCount") as!Int
                        questionObject.textOptionOne = que.objectForKey("textOptionOne") as!String
                        questionObject.textOptionTwo = que.objectForKey("textOptionTwo") as!String
                        questionObject.textOptionThree = que.objectForKey("textOptionThree") as!String
                        questionObject.textOptionFour = que.objectForKey("textOptionFour") as!String
                        questionObject.imageOptionOne = que.objectForKey("imageOptionOne") as? PFFile
                        questionObject.imageOptionTwo = que.objectForKey("imageOptionTwo") as? PFFile
                        questionObject.imageOptionThree = que.objectForKey("imageOptionThree") as? PFFile
                        questionObject.imageOptionFour = que.objectForKey("imageOptionFour") as? PFFile
                        
                        
                        
                        
                        
                        //check question expiry date + settings visible hrs after expiry
                        
                        
                        let dateFormatter = NSDateFormatter()
                        
                        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
                        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "dd/MM/yyyyy hh:mm a"
                        print(questionObject.expiryDate)
                        let str:String = questionObject.expiryDate as! String
                        var dateobj = dateFormatter.dateFromString(str)
                        var hrsToAdd:Double = 1
                        if ((self.appDelegateObj.settingObject) != nil){
                            hrsToAdd = (self.appDelegateObj.settingObject.questionInInboxVisibility?.doubleValue)!;
                        }
                        else{
                            hrsToAdd = 1
                        }
                        dateobj = dateobj?.dateByAddingTimeInterval(60*60*hrsToAdd)
                        if dateobj!.compare(NSDate()) == NSComparisonResult.OrderedDescending
                        {
                            self.items.append(questionObject)
                        }
                        
                        
                        
                        //                        self.parseCallToRetriveAnswer()
                        
                    }
                }
                self.appDelegateObj.hideIndicatorView()
                
                self.tableViewInbx.reloadData()
                
                self.coredata.saveQuestionsArrayIntoDatabase(self.items)
            }
            else{
                print("Error in retrieving \(error)")
            }
            
        })
    }
    func InboxViewRefresh(){
        self.searchbarInbox.resignFirstResponder()
        self.items.removeAll()
        self.tableViewInbx.reloadData()
        if (Reachability.isConnectedToNetwork()){
            self.parseCallToGetInboxData()
        }
        else{
            self.items = coredata.getQuestionsDataFromDatabase(true, owner: "")
            print("inbox offiline data")
            print(self.items)
            self.tableViewInbx.reloadData()
        }
        
       
    }
    
    func ButtonInboxTapped(sender:UIButton!)
    {
        //        btn.backgroundColor = UIColor(red: 37/255, green: 64/255, blue: 98/255, alpha: 1)
        
        NSNotificationCenter.defaultCenter().postNotificationName("InboxTapped", object: nil)

    }
    

    override func viewDidAppear(animated: Bool) {
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        print("view did appear in inbox tablevc")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filteredArray.count
        }
        else
        {
        return items.count;
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:QuestionCell = self.tableViewInbx.dequeueReusableCellWithIdentifier("cellnbox") as!QuestionCell

        let questionObject:QuestionModel! = self.items[indexPath.row]
        
        let firstName: String = questionObject.question as!String
        cell.questionLabel?.text = firstName
        let optionCount: NSNumber = questionObject.optionCount as!Int
        let questionType: NSNumber = questionObject.isType as!Int
        if(questionType == 1){ //text question
            cell.viewWithTextOptions.hidden = false;
            for view in cell.viewWithTextOptions.subviews{
                if view.isKindOfClass(UILabel) && view.tag == 1{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionOne {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                }
                if view.isKindOfClass(UILabel) && view.tag == 2{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionTwo {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                }
                if view.isKindOfClass(UILabel) && view.tag == 3{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionThree {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                }
                if view.isKindOfClass(UILabel) && view.tag == 4{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionFour {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                    
                }
            }
        }
        else if(questionType == 2){ //image question
            if(optionCount == 2){
                cell.viewWithTwoOptionsImage.hidden = false;
                for view in cell.viewWithTwoOptionsImage.subviews{
                    if view.isKindOfClass(UIImageView) && view.tag == 1{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionOne {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 2{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionTwo {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                }
            }
            else if(optionCount == 3){
                cell.viewWithThreeOptionsImage.hidden = false;
                for view in cell.viewWithThreeOptionsImage.subviews{
                    if view.isKindOfClass(UIImageView) && view.tag == 1{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionOne {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 2{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionTwo {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 3{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionThree {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                }
                
            }
            else if(optionCount == 4){
                cell.viewWithFourOptionsImage.hidden = false;
                for view in cell.viewWithFourOptionsImage.subviews{
                    if view.isKindOfClass(UIImageView) && view.tag == 1{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionOne {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 2{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionTwo {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 3{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionThree {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 4{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionFour {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                }
            }
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let inboxVCObject = storyBoard.instantiateViewControllerWithIdentifier("InboxViewController") as!InboxViewController
        var questionObject:QuestionModel!
        var answerObject:AnswerModel!
        questionObject = self.items[indexPath.row]
        for var i = 0; i < self.answerArray.count; i++
        {
            answerObject = self.answerArray[i]
            
            if answerObject.questionId == questionObject.objectId{
                break
            }
        }
        inboxVCObject.GLobalanswerObjectITVC = answerObject
        inboxVCObject.GlobalquestionObjectITVC = questionObject
        inboxVCObject.allQuestionsModel = self.items
        inboxVCObject.selectedIndex = indexPath.row as Int
            self.navigationController?.pushViewController(inboxVCObject, animated: false)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    
    // MARK: search bar delegates--------------------
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText == ""
        {
            searchActive = false
        }
        else
        {
            searchActive = true
            let searchPredicate = NSPredicate(format: "question CONTAINS[c] %@", searchText)
            let array:[QuestionModel]! = (self.items as NSArray).filteredArrayUsingPredicate(searchPredicate) as![QuestionModel]
            filteredArray = array

        }
        self.tableViewInbx.reloadData()
    }

    
    @IBAction func btnCameraClicked(sender: UIButton)
    {
    }

}
 
 
 
 /* func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
 {
 var cell:QuestionCell = self.tableViewInbx.dequeueReusableCellWithIdentifier("cellnbox") as!QuestionCell
 var questionObject:QuestionModel!
 var answerObject:AnswerModel!
 
 
 if(searchActive)
 {
 questionObject = self.filteredArray[indexPath.row]
 for var i = 0; i < self.answerArray.count; i++
 {
 answerObject = self.answerArray[i]
 
 if answerObject.questionId == questionObject.objectId{
 break
 }
 }
 
 
 }
 else
 {
 questionObject = self.items[indexPath.row]
 for var i = 0; i < self.answerArray.count; i++
 {
 answerObject = self.answerArray[i]
 
 if answerObject.questionId == questionObject.objectId{
 break
 }
 }
 }
 
 
 
 /*  for answerObject1 in self.answerArray
 {
 if answerObject1.questionId == questionObject.objectId{
 break
 }
 println("answerobject in retrievinfdfa \(answerObject1)")
 }
 */
 
 let firstName: String = questionObject.question as!String
 cell.questionLabel?.text = firstName
 let optionCount: NSNumber = questionObject.optionCount as!Int
 let questionType: NSNumber = questionObject.isType as!Int
 if(questionType == 1)
 { //text question
 cell.viewWithTextOptions.hidden = false;
 for view in cell.viewWithTextOptions.subviews
 {
 if view.isKindOfClass(UILabel) && view.tag == 1{
 let vw:UILabel = view as!UILabel
 if let text = answerObject.textOptionOne {
 vw.text = text as!String
 }
 else{
 vw.hidden = true
 }
 }
 if view.isKindOfClass(UILabel) && view.tag == 2{
 let vw:UILabel = view as!UILabel
 if let text = answerObject.textOptionTwo {
 vw.text = text as!String
 }
 else{
 vw.hidden = true
 }
 }
 if view.isKindOfClass(UILabel) && view.tag == 3{
 let vw:UILabel = view as UILabel
 if let text = answerObject.textOptionThree {
 vw.text = text as String
 }
 else{
 vw.hidden = true
 }
 }
 if view.isKindOfClass(UILabel) && view.tag == 4{
 let vw:UILabel = view as UILabel
 if let text = answerObject.textOptionFour {
 vw.text = text as String
 }
 else{
 vw.hidden = true
 }
 
 }
 }
 }
 else if(questionType == 2)
 { //image question
 if(optionCount == 2){
 cell.viewWithTwoOptionsImage.hidden = false;
 for view in cell.viewWithTwoOptionsImage.subviews{
 if view.isKindOfClass(UIImageView) && view.tag == 1{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionOne {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 if view.isKindOfClass(UIImageView) && view.tag == 2{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionTwo {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 }
 }
 else if(optionCount == 3){
 cell.viewWithThreeOptionsImage.hidden = false;
 for view in cell.viewWithThreeOptionsImage.subviews{
 if view.isKindOfClass(UIImageView) && view.tag == 1{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionOne {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 if view.isKindOfClass(UIImageView) && view.tag == 2{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionTwo {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 if view.isKindOfClass(UIImageView) && view.tag == 3{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionThree {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 }
 
 }
 else if(optionCount == 4){
 cell.viewWithFourOptionsImage.hidden = false;
 for view in cell.viewWithFourOptionsImage.subviews{
 if view.isKindOfClass(UIImageView) && view.tag == 1{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionOne {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 if view.isKindOfClass(UIImageView) && view.tag == 2{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionTwo {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 if view.isKindOfClass(UIImageView) && view.tag == 3{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionThree {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 if view.isKindOfClass(UIImageView) && view.tag == 4{
 let vw:UIImageView = view as UIImageView
 if let userPicture = answerObject.imageOptionFour {
 userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
 if (error == nil) {
 vw.image = UIImage(data:imageData!)
 }
 }
 }
 }
 }
 }
 }
 GlobalquestionObject = questionObject
 GLobalanswerObject = answerObject
 return cell
 }*/
 
 
 /* func parseCallToRetriveAnswer(){
 var compQuery = PFQuery(className:"Answers")
 // compQuery.whereKey("owner", equalTo:"1")
 compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
 if(error == nil){
 if let answerObjects = results as? [PFObject] {
 for answer in answerObjects {
 // Use staff as a standard PFObject now. e.g.
 let optionOne: AnyObject? = answer.objectForKey("textOptionOne")
 var ansObj = AnswerModel()
 // ansObj.objectId = answer.objectForKey("objectId") as! String
 ansObj.questionId = answer.objectForKey("questionId") as String
 ansObj.isType = answer.objectForKey("isType") as Int
 ansObj.textOptionOne = answer.objectForKey("textOptionOne") as String
 ansObj.textOptionTwo = answer.objectForKey("textOptionTwo") as String
 ansObj.textOptionThree = answer.objectForKey("textOptionThree") as String
 ansObj.textOptionFour = answer.objectForKey("textOptionFour") as String
 ansObj.imageOptionOne = answer.objectForKey("imageOptionOne") as? PFFile
 ansObj.imageOptionTwo = answer.objectForKey("imageOptionTwo") as? PFFile
 ansObj.imageOptionThree = answer.objectForKey("imageOptionThree") as? PFFile
 ansObj.imageOptionFour = answer.objectForKey("imageOptionFour") as? PFFile
 ansObj.userId = answer.objectForKey("userId") as String
 
 self.answerArray.append(ansObj)
 }
 }
 [self.tableViewInbx .reloadData()]
 }
 else{
 println("Error in retrieving \(error)")
 }
 
 })
 }*/

