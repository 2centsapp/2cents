//
//  AskQueViewController.swift
//  CentsApp
//
//  Created by Mahesh on 11/3/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import AddressBook
import MobileCoreServices
import Parse


class AskQueViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnPhotos: UIButton!
    @IBOutlet var imgView1: UIImageView!
    @IBOutlet var imgView2: UIImageView!
    @IBOutlet var imgView3: UIImageView!
    @IBOutlet var imgView4: UIImageView!
    
    @IBOutlet var txtFld1: UITextField!
    @IBOutlet var txtFld2: UITextField!
    @IBOutlet var txtFld3: UITextField!
    @IBOutlet var txtFld4: UITextField!
    
    @IBOutlet var txtView: UITextView!
    @IBOutlet var datepicker: UIDatePicker!
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnShowPicker: UIButton!
    var newMedia: Bool?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btn   = UIButton(type:UIButtonType.System)
        btn.frame = CGRectMake(0, 0, self.view.frame.size.width,65)
        btn.setTitle("Ask A Question", forState: .Normal)  //set button title
        btn.setTitleColor(UIColor.blackColor(),forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.addTarget(self, action: "ButtonASkQueTapped:", forControlEvents: .TouchUpInside) //add button action
        
        self.view.addSubview(btn)
        
        let maskPath = UIBezierPath(roundedRect:self.view.bounds, byRoundingCorners: [UIRectCorner.TopLeft , UIRectCorner.TopRight], cornerRadii: CGSizeMake(10.0, 10.0))
        
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.CGPath
        self.view.layer.mask = maskLayer
        self.view.addSubview(btn)
        
        // Import Contacts from device
        datepicker.hidden = true;
        btnShowPicker.tag = 0;
        self.lblDate.text = ""
        self.clearAllQuestionDataFromView()
    }
    
    func ButtonASkQueTapped(sender:UIButton!){
        
        self.txtFld1.resignFirstResponder()
        self.txtFld2.resignFirstResponder()
        self.txtFld3.resignFirstResponder()
        self.txtFld4.resignFirstResponder()
        self.txtView.resignFirstResponder()

        
        NSNotificationCenter.defaultCenter().postNotificationName("AskQuestionsTapped", object: nil)
    }

    


    
    /* -------------------------------Sagar's code for Ask questions  ------------------------*/
    @IBAction func btnCameraClicked(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = false
                                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
        
    }
    @IBAction func btnGalleryClicked(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.PhotoLibrary) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        self.dismissViewControllerAnimated(true, completion: nil)
        if mediaType == (kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            if((self.imgView1.image) == nil){
                self.imgView1.image  = image
            }
            else if( (self.imgView2.image) == nil){
                self.imgView2.image  = image
            }
            else if((self.imgView3.image) == nil){
                self.imgView3.image  = image
            }
            else if((self.imgView4.image) == nil){
                self.imgView4.image  = image
            }
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self,
                    "image:didFinishSavingWithError:contextInfo:", nil)
            } else if mediaType == (kUTTypeMovie as String) {
                // Code to support video here
            }
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .Cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true,
                completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnSaveClicked(sender: UIButton){
        self.txtFld1.resignFirstResponder()
        self.txtFld2.resignFirstResponder()
        self.txtFld3.resignFirstResponder()
        self.txtFld4.resignFirstResponder()
        self.txtView.resignFirstResponder()
        
        
        
        
        if (Reachability.isConnectedToNetwork()){
            if (self.isValidate()){
                self.appDelegateObj.showIndicatorView()
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
                dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                dateFormatter.dateFormat = "dd/MM/yyyyy hh:mm a"
                let strDate = dateFormatter.stringFromDate(NSDate())
                
                var post:PFObject!
                post  = PFObject(className: "Questions")
                post["question"] = self.txtView.text
                
                
                post["textAnswer"] = ""
                post["expiryTime"] = 180
                post["expiryDate"] = self.lblDate.text
                //post["expiryDate"] = "28/01/2015 11:38 AM"
                post["questionDate"] = strDate
                post["owner"] = self.appDelegateObj.loggedInUser?.objectId
                if(self.txtFld1.text!.isEmpty){
                    post["isType"] = 2
                    
                    if((self.imgView1.image) != nil){
                        let imageData = UIImageJPEGRepresentation(self.imgView1.image!,1.0)
                        let imageFile:PFFile = PFFile(name: "image", data: imageData!)
                        post["imageOptionOne"] = imageFile
                        post["optionCount"] = 1
                        
                    }
                    if((self.imgView2.image) != nil){
                        let imageData = UIImageJPEGRepresentation(self.imgView2.image!,1.0)
                        let imageFile:PFFile = PFFile(name: "image", data: imageData!)
                        post["imageOptionTwo"] = imageFile
                        post["optionCount"] = 2
                        
                    }
                    if((self.imgView3.image) != nil){
                        let imageData = UIImageJPEGRepresentation(self.imgView3.image!,1.0)
                        let imageFile:PFFile = PFFile(name: "image", data: imageData!)
                        post["imageOptionThree"] = imageFile
                        post["optionCount"] = 3
                        
                    }
                    if((self.imgView4.image) != nil){
                        let imageData = UIImageJPEGRepresentation(self.imgView4.image!,1.0)
                        let imageFile:PFFile = PFFile(name: "image", data: imageData!)
                        post["imageOptionFour"] = imageFile
                        post["optionCount"] = 4
                    }
                    
                    post["textOptionOne"] = ""
                    post["textOptionTwo"] = ""
                    post["textOptionThree"] = ""
                    post["textOptionFour"] = ""
                    
                }else{
                    post["isType"] = 1
                    post["textOptionOne"] = self.txtFld1.text
                    post["textOptionTwo"] = self.txtFld2.text
                    post["textOptionThree"] = self.txtFld3.text
                    post["textOptionFour"] = self.txtFld4.text
                    post["optionCount"] = 4
                }
                post.saveInBackgroundWithBlock{
                    (success:Bool, error:NSError?)-> Void in
                    if success == true{
                        print("question uploaded success")
                        print(post.objectId)
                        let alert = UIAlertController(title: "Success", message: "Your question published successfully", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:self.cancelAction))
                        self.presentViewController(alert, animated: true, completion: {
                            print("completion block")
                            
                            self.clearAllQuestionDataFromView()
                        })
                    }
                    else{
                        print("uploaded failed")
                    }
                    self.appDelegateObj.hideIndicatorView()
                    
                }
            }
            
        }
        else{
            let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
            alertView.show()
        }
       

    }
    func clearAllQuestionDataFromView(){
        self.txtView.text = ""
        self.txtFld1.text = ""
        self.txtFld2.text = ""
        self.txtFld3.text = ""
        self.txtFld4.text = ""
        self.imgView1.image = nil
        self.imgView2.image = nil
        self.imgView3.image = nil
        self.imgView4.image = nil
        
        self.lblDate.text = ""

        
    }
    func cancelAction(alertView: UIAlertAction!){
        print("Cancelled !!")
    }
    
    
    
 /*   func textFieldDidBeginEditing(textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        // self.txtFld1.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing(textField: UITextView) {    //delegate method
        
    }
    
    func textViewShouldEndEditing(textField: UITextView) -> Bool {  //delegate method
        return true
    }
    
   */
    func textViewShouldReturn(textField: UITextView) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    @IBAction func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd/MM/yyyyy hh:mm a"
        
        let strDate = dateFormatter.stringFromDate(datePicker.date)
        lblDate.text = strDate
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnShowPickerCLicked(sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            datepicker.hidden = false
            if (self.appDelegateObj.settingObject != nil){
                if (self.appDelegateObj.settingObject.questionExpiryTimer != nil){
                    var hrsToAdd:Double = 1
                    hrsToAdd = (self.appDelegateObj.settingObject.questionExpiryTimer?.doubleValue)!;
                    datepicker.minimumDate = NSDate().dateByAddingTimeInterval(60*60*hrsToAdd)

                }
                else{
                    datepicker.minimumDate = NSDate()

                }
            }
            else
            {
                datepicker.minimumDate = NSDate()
            }
            
        }
        else if sender.tag == 1{
            sender.tag = 0
            datepicker.hidden = true
        }
        
        
        
        
    }
    
    func isValidate()->Bool{
        if (txtView.text?.characters.count == 0){
            let alert = UIAlertController(title: "Alert", message:"Please enter question.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
            })
            return false
        }
        else if (txtFld1.text?.characters.count == 0 && imgView1.image == nil){
            let alert = UIAlertController(title: "Alert", message:"Please add first option for your question.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
            })
            return false
        }
        else if (((txtFld1.text?.characters.count > 0) && txtFld2.text?.characters.count == 0 && txtFld3.text?.characters.count == 0 && txtFld4.text?.characters.count == 0) || (imgView1.image != nil && imgView2.image == nil && imgView3.image == nil && imgView4.image == nil)){
            let alert = UIAlertController(title: "Alert", message:"Please add 2 or more options for your question.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: {
            })
            return false
        }
        else if (self.lblDate.text == ""){
            
            let myAlert = UIAlertController(title:"Alert", message:"Please select expiry date for your question.", preferredStyle: UIAlertControllerStyle.Alert);
            let okAction =  UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
            myAlert.addAction(okAction);
            self.presentViewController(myAlert, animated:true, completion:nil);
            return false
            
        }
        
        return true
    }
}
