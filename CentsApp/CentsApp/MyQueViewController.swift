//
//  MyQueViewController.swift
//  CentsApp
//
//  Created by Mahesh on 11/3/15.
//  Copyright (c) 2015 Rapidera. All rights reserved.
//

import UIKit
import Foundation
import Parse

class MyQueViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    let appDelegateObj = UIApplication.sharedApplication().delegate as! AppDelegate
   // @IBOutlet weak var searchbarInbox: UISearchBar!
    @IBOutlet var tableViewMyQuestions: UITableView!
    var items: [QuestionModel]! = []
    var answerArray: [AnswerModel]! = []
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "MyQuestionsViewRefresh", name: "MyQuestionsRefresh", object: nil)
        
        let btn   = UIButton(type:UIButtonType.System)
        btn.frame = CGRectMake(0, 0, self.view.frame.size.width,65)
        btn.setTitle("My Questions", forState: .Normal)  //set button title
        btn.setTitleColor(UIColor.blackColor(),forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.addTarget(self, action: "buttonMYQTapped:", forControlEvents: .TouchUpInside) //add button action
        
        self.view.addSubview(btn)
        
        let maskPath = UIBezierPath(roundedRect:self.view.bounds, byRoundingCorners: [UIRectCorner.TopLeft , UIRectCorner.TopRight], cornerRadii: CGSizeMake(10.0, 10.0))
        
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.CGPath
        self.view.layer.mask = maskLayer
        self.view.addSubview(btn)
        
        
         tableViewMyQuestions.delegate = self
        
        
        if (Reachability.isConnectedToNetwork()){
            let compQuery = PFQuery(className:"Questions")
            compQuery.whereKey("owner", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
            compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
                if(error == nil){
                    if let questionsObjects:NSArray = results {
                        for question in questionsObjects {
                            // Use staff as!a standard PFObject now. e.g.
                            let que:PFObject = question as! PFObject
                            
                            let questionObject = QuestionModel()
                            questionObject.objectId = que.objectId
                            questionObject.question = que.objectForKey("question") as!String
                            questionObject.isType = que.objectForKey("isType") as!Int
                            questionObject.textAnswer = que.objectForKey("textAnswer") as!String
                            questionObject.expiryTime = que.objectForKey("expiryTime") as!Int
                            questionObject.expiryDate = que.objectForKey("expiryDate") as!String
                            questionObject.questionDate = que.objectForKey("questionDate") as!String
                            questionObject.owner = que.objectForKey("owner") as!String
                            questionObject.optionCount = que.objectForKey("optionCount") as!Int
                            questionObject.textOptionOne = que.objectForKey("textOptionOne") as!String
                            questionObject.textOptionTwo = que.objectForKey("textOptionTwo") as!String
                            questionObject.textOptionThree = que.objectForKey("textOptionThree") as!String
                            questionObject.textOptionFour = que.objectForKey("textOptionFour") as!String
                            questionObject.imageOptionOne = que.objectForKey("imageOptionOne") as? PFFile
                            questionObject.imageOptionTwo = que.objectForKey("imageOptionTwo") as? PFFile
                            questionObject.imageOptionThree = que.objectForKey("imageOptionThree") as? PFFile
                            questionObject.imageOptionFour = que.objectForKey("imageOptionFour") as? PFFile
                            
                            let dateFormatter = NSDateFormatter()
                            
                            dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
                            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "dd/MM/yyyyy hh:mm a"
                            print(questionObject.expiryDate)
                            let str:String = questionObject.expiryDate as! String
                            var dateobj = dateFormatter.dateFromString(str)
                            var hrsToAdd:Double = 1
                            if ((self.appDelegateObj.settingObject) != nil){
                                hrsToAdd = (self.appDelegateObj.settingObject.questionInMyQuestionVisibility?.doubleValue)!;
                            }
                            else{
                                hrsToAdd = 1
                            }
                            dateobj = dateobj?.dateByAddingTimeInterval(60*60*hrsToAdd)
                            if dateobj!.compare(NSDate()) == NSComparisonResult.OrderedDescending
                            {
                                self.items.append(questionObject)
                            }
                        }
                        print("myquestions:::\(self.items)");
                        self.tableViewMyQuestions.reloadData()
                    }
                }
                else{
                    print("Error in retrieving \(error)")
                }
                //self.appDelegateObj.hideIndicatorView()
                
            })

        }
        else{
            let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
            alertView.show()
        }
        
        
        
    }
    func MyQuestionsViewRefresh(){
        self.items.removeAll()
        self.tableViewMyQuestions.reloadData()
        if (Reachability.isConnectedToNetwork()){
            let compQuery = PFQuery(className:"Questions")
            compQuery.whereKey("owner", equalTo:(self.appDelegateObj.loggedInUser?.objectId)!)
            compQuery.findObjectsInBackgroundWithBlock( { (NSArray results, NSError error) in
                if(error == nil){
                    if let questionsObjects:NSArray = results {
                        for question in questionsObjects {
                            // Use staff as!a standard PFObject now. e.g.
                            let que:PFObject = question as! PFObject
                            
                            let questionObject = QuestionModel()
                            questionObject.objectId = que.objectId
                            questionObject.question = que.objectForKey("question") as!String
                            questionObject.isType = que.objectForKey("isType") as!Int
                            questionObject.textAnswer = que.objectForKey("textAnswer") as!String
                            questionObject.expiryTime = que.objectForKey("expiryTime") as!Int
                            questionObject.expiryDate = que.objectForKey("expiryDate") as!String
                            questionObject.questionDate = que.objectForKey("questionDate") as!String
                            questionObject.owner = que.objectForKey("owner") as!String
                            questionObject.optionCount = que.objectForKey("optionCount") as!Int
                            questionObject.textOptionOne = que.objectForKey("textOptionOne") as!String
                            questionObject.textOptionTwo = que.objectForKey("textOptionTwo") as!String
                            questionObject.textOptionThree = que.objectForKey("textOptionThree") as!String
                            questionObject.textOptionFour = que.objectForKey("textOptionFour") as!String
                            questionObject.imageOptionOne = que.objectForKey("imageOptionOne") as? PFFile
                            questionObject.imageOptionTwo = que.objectForKey("imageOptionTwo") as? PFFile
                            questionObject.imageOptionThree = que.objectForKey("imageOptionThree") as? PFFile
                            questionObject.imageOptionFour = que.objectForKey("imageOptionFour") as? PFFile
                            
                            let dateFormatter = NSDateFormatter()
                            
                            dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
                            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "dd/MM/yyyyy hh:mm a"
                            print(questionObject.expiryDate)
                            let str:String = questionObject.expiryDate as! String
                            var dateobj = dateFormatter.dateFromString(str)
                            var hrsToAdd:Double = 1
                            if ((self.appDelegateObj.settingObject) != nil){
                                hrsToAdd = (self.appDelegateObj.settingObject.questionInMyQuestionVisibility?.doubleValue)!;
                            }
                            else{
                                hrsToAdd = 1
                            }
                            dateobj = dateobj?.dateByAddingTimeInterval(60*60*hrsToAdd)
                            if dateobj!.compare(NSDate()) == NSComparisonResult.OrderedDescending
                            {
                                self.items.append(questionObject)
                            }
                            // self.tableViewMyQuestions.reloadData()
                        }
                        print(self.items);
                        if self.items.count == 0{
                            let alert = UIAlertController(title: "Sorry", message:"You have not asked any question yet.", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler:nil))
                            self.presentViewController(alert, animated: true, completion: {
                                
                            })
                            
                        }
                        self.tableViewMyQuestions.reloadData()
                    }
                }
                else{
                    print("Error in retrieving \(error)")
                }
                //self.appDelegateObj.hideIndicatorView()
                
            })
        }
        else{
            let alertView = UIAlertView(title: "Sorry", message: "Internet connection is not available", delegate: nil, cancelButtonTitle: "OK")
            alertView.show()
        }
     }
    func buttonMYQTapped(sender:UIButton!)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("MYQTapped", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:QuestionCell = self.tableViewMyQuestions.dequeueReusableCellWithIdentifier("cellnbox") as!QuestionCell
        
        let questionObject:QuestionModel! = self.items[indexPath.row]
        
        let firstName: String = questionObject.question as!String
        cell.questionLabel?.text = firstName
        let optionCount: NSNumber = questionObject.optionCount as!Int
        let questionType: NSNumber = questionObject.isType as!Int
        if(questionType == 1){ //text question
            cell.viewWithTextOptions.hidden = false;
            for view in cell.viewWithTextOptions.subviews{
                if view.isKindOfClass(UILabel) && view.tag == 1{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionOne {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                }
                if view.isKindOfClass(UILabel) && view.tag == 2{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionTwo {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                }
                if view.isKindOfClass(UILabel) && view.tag == 3{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionThree {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                }
                if view.isKindOfClass(UILabel) && view.tag == 4{
                    let vw:UILabel = view as!UILabel
                    if let text = questionObject.textOptionFour {
                        vw.text = text as String
                    }
                    else{
                        vw.hidden = true
                    }
                    
                    
                    
                    
                }
            }
        }
        else if(questionType == 2){ //image question
            if(optionCount == 2){
                cell.viewWithTwoOptionsImage.hidden = false;
                for view in cell.viewWithTwoOptionsImage.subviews{
                    if view.isKindOfClass(UIImageView) && view.tag == 1{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionOne {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 2{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionTwo {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                }
            }
            else if(optionCount == 3){
                cell.viewWithThreeOptionsImage.hidden = false;
                for view in cell.viewWithThreeOptionsImage.subviews{
                    if view.isKindOfClass(UIImageView) && view.tag == 1{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionOne {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 2{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionTwo {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 3{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionThree {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                }
                
            }
            else if(optionCount == 4){
                cell.viewWithFourOptionsImage.hidden = false;
                for view in cell.viewWithFourOptionsImage.subviews{
                    if view.isKindOfClass(UIImageView) && view.tag == 1{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionOne {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 2{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionTwo {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 3{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionThree {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                    if view.isKindOfClass(UIImageView) && view.tag == 4{
                        let vw:UIImageView = view as!UIImageView
                        if let userPicture = questionObject.imageOptionFour {
                            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                                if (error == nil) {
                                    vw.image = UIImage(data:imageData!)
                                }
                            }
                        }
                    }
                }
            }
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let inboxVCObject = storyBoard.instantiateViewControllerWithIdentifier("InboxViewController") as!InboxViewController
        var questionObject:QuestionModel!
        var answerObject:AnswerModel!
        questionObject = self.items[indexPath.row]
        for var i = 0; i < self.answerArray.count; i++
        {
            answerObject = self.answerArray[i]
            
            if answerObject.questionId == questionObject.objectId{
                break
            }
        }
        inboxVCObject.GLobalanswerObjectITVC = answerObject
        inboxVCObject.GlobalquestionObjectITVC = questionObject
        inboxVCObject.allQuestionsModel = self.items
        inboxVCObject.selectedIndex = indexPath.row as Int
        self.navigationController?.pushViewController(inboxVCObject, animated: false)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }


}
