//
//  AnswerModel.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 11/19/15.
//  Copyright (c) 2015 Sagar Jadhav. All rights reserved.
//

import UIKit
import Parse

class AnswerModel: NSObject {
    var objectId:NSString?
    var questionId:NSString?
    var textOptionOne:NSString?
    var textOptionTwo:NSString?
    var textOptionThree:NSString?
    var textOptionFour:NSString?
    var imageOptionOne:PFFile?
    var imageOptionTwo:PFFile?
    var imageOptionThree:PFFile?
    var imageOptionFour:PFFile?
    var userId:NSString?
    var isType:NSNumber?
   
}
