//
//  QuestionModel.swift
//  CentsApp
//
//  Created by Sagar Jadhav on 11/19/15.
//  Copyright (c) 2015 Sagar Jadhav. All rights reserved.
//
import UIKit
import Foundation
import Parse

class QuestionModel: NSObject {
    var objectId:NSString?
    var question:NSString?
    var isType:NSNumber?
    var textAnswer:NSString?
    var expiryTime:NSNumber?
    var questionDate:NSString?
    var owner:NSString?
    var optionCount:NSNumber?
    var expiryDate:NSString?
    var textOptionOne:NSString?
    var textOptionTwo:NSString?
    var textOptionThree:NSString?
    var textOptionFour:NSString?
    var imageOptionOne:PFFile?
    var imageOptionTwo:PFFile?
    var imageOptionThree:PFFile?
    var imageOptionFour:PFFile?
    
}
